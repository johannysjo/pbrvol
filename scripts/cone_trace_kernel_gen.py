"""
.. module:: cone_trace_kernel_gen
   :platform: Unix
   :synopsis: Cone tracing kernel generator.

.. moduleauthor:: Johan Nysjo

Provides functions for generating and visualizing sampling kernels for
cone tracing.

"""

import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def vogel_sphere(n):
    """Generates n points that are evenly distributed over a sphere.
    Reference: http://blog.marmakoide.org/?p=1

    Args:
        n: An integer specifying the number of points to generate.

    Returns:
        An n-by-3 NumPy array representing the points.

    """
    golden_angle = numpy.pi * (3 - numpy.sqrt(5))
    theta = golden_angle * numpy.arange(n)
    z = numpy.linspace(1 - 1.0 / n, 1.0 / n - 1, n)
    radius = numpy.sqrt(1 - z * z)

    points = numpy.zeros((n, 3))
    points[:, 0] = radius * numpy.cos(theta)
    points[:, 1] = radius * numpy.sin(theta)
    points[:, 2] = z

    return points


def generate_cone_tracing_kernel(cone_angle):
    """Generates a spherical sampling kernel for cone tracing.

    Args:
        cone_angle: A float specifying the cone angle in radians.

    Returns:
        An n-by-3 NumPy array with n cone directions.

    """
    disc_radius = numpy.sin(0.5 * cone_angle)
    disc_area = numpy.pi * disc_radius * disc_radius
    sphere_area = 4.0 * numpy.pi
    max_density = 0.9
    num_cones = int(numpy.ceil(max_density * sphere_area / disc_area))
    cones = vogel_sphere(num_cones)

    return cones


def cones2glsl(cones):
    """Converts a cone tracing kernel into GLSL code.

    Args:
        cones: An n-by-3 NumPy array with n cone directions.

    Returns:
        A string with GLSL code.

    """
    num_cones = cones.shape[0]
    glsl_str = "vec3 cone_directions[%d];\n" % num_cones
    for i in xrange(0, num_cones):
        glsl_str += ("cone_directions[%d] = vec3(%f, %f, %f);\n" %
                     (i, cones[i, 0], cones[i, 1], cones[i, 2]))

    return glsl_str


def plot_cones(cones):
    """Shows a 3D scatter plot of the cone directions in a cone tracing
    kernel."""
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(cones[:, 0], cones[:, 1], cones[:, 2])
    ax.set_aspect("equal")
    plt.show()


def main():
    cone_angle = numpy.pi * 60.0 / 180.0
    cones = generate_cone_tracing_kernel(cone_angle)

    print(cones2glsl(cones))
    plot_cones(cones)


if __name__ == "__main__":
    main()
