"""
.. module:: volume
   :platform: Unix
   :synopsis: Volume image module.

.. moduleauthor:: Johan Nysjo

Provides classes and functions for working with volumetric images.

Note: We use Fortran (F) order NumPy arrays to store volume image data
and assume that this order is used consistently in the application.

"""

import sys
import copy
import logging
import os

import numpy


def load_vtk_volume(filename):
    """Loads a VTK volume from file into a Volume object."""
    logging.info("Loading volume from " + filename + " ...")
    reader = VTKReader()
    reader.filename = filename
    reader.update()
    volume = Volume()
    volume.image_data = reader.image_data
    volume.origin = reader.origin
    volume.spacing = reader.spacing
    logging.info("Done!\n")
    return volume


def save_vtk_volume(volume, filename):
    """Saves a Volume object to a VTK file."""
    logging.info("Writing volume to " + filename + " ...")
    writer = VTKWriter()
    writer.filename = filename
    writer.image_data = volume.image_data
    writer.dimensions = volume.dimensions
    writer.origin = volume.origin
    writer.spacing = volume.spacing
    writer.write()
    logging.info("Done!\n")


def int16_to_uint8(volume, min_hu=-1024, max_hu=3071):
    """Converts an int16 CT volume with intensity values [-1024, 3071]
    to an uint8 volume with intensity values [0, 255]."""
    scalars = volume.image_data.astype(numpy.float32)
    scalars[scalars < min_hu] = min_hu
    scalars[scalars > max_hu] = max_hu
    scalars = (scalars - min_hu) / (max_hu - min_hu)
    scalars = (255 * scalars).astype(numpy.uint8)
    volume_uint8 = Volume()
    volume_uint8.image_data = scalars
    volume_uint8.origin = copy.copy(volume.origin)
    volume_uint8.spacing = copy.copy(volume.spacing)
    return volume_uint8


class Volume(object):
    """Represents a volume image.

    Attributes:
        image_data: A MxNxP uint8 NumPy array representing the volume
            image data.
        origin: A 3-element list of integers specifying the origin
            (ox,oy,oz) of the image data.
        spacing: A 3-element list of floats specifying the voxel
            spacing (sx,sy,sz).
        dimensions: A 3-element list of integers specifying the
            dimensions (width,height,depth) of the image
            data. Read-only.

    Usage:
    >>> volume = Volume()
    >>> volume.image_data = numpy.zeros((3, 3, 3), dtype=numpy.uint8, order='F')
    >>> volume.origin = [0, 0, 0]
    >>> volume.spacing = [1.0, 1.0, 1.0]

    """

    def __init__(self):
        self._image_data = None
        self.origin = None
        self.spacing = None
        self._dimensions = None

    @property
    def image_data(self):
        return self._image_data

    @image_data.setter
    def image_data(self, value):
        self._image_data = value
        self._dimensions = value.shape

    @property
    def dimensions(self):
        return self._dimensions


class VTKReader(object):
    """Reads an uint8, uint16, or int16 volume image from a VTK file
    into a Fortran (F) order 3D NumPy array.

    Attributes:
        filename: The name of the VTK file to be read.
        image_data: A MxNxP uint8, uint16, or int16 NumPy array
            representing the volume image data.
        dimensions: A 3-element list of integers specifying the
            dimensions (width,height,depth) of the image data.
        spacing: A 3-element list of floats specifying the voxel
            spacing (sx,sy,sz).
        origin: A 3-element list of integers specifying the origin
            (ox,oy,oz) of the image data.
        data_type: A string specifying the data type of the voxels.

    Usage:
    >>> reader = VTKReader()
    >>> reader.filename = 'input.vtk'
    >>> reader.update()
    >>> image_data = reader.image_data
    >>> dimensions = reader.dimensions
    >>> spacing = reader.spacing
    >>> orgin = reader.origin

    """

    def __init__(self):
        self.filename = ""
        self.image_data = None
        self.dimensions = [0, 0, 0]
        self.spacing = [1.0, 1.0, 1.0]
        self.origin = [0, 0, 0]
        self.data_type = None

    def update(self):
        """Executes the reader."""
        if self.filename:
            header = self._read_header()
            self._extract_meta_data_from_header(header)
            self._read_data()

    def _read_header(self):
        header = []
        number_of_header_lines = 10
        with open(self.filename, 'rb') as vtk_file:
            for i, line in enumerate(vtk_file):
                if i >= number_of_header_lines:
                    break
                header.append(line)
        return header

    def _extract_meta_data_from_header(self, header):
        assert(len(header) == 10)
        processed_header = list(header)
        processed_header = [line.rstrip("\n") for line in processed_header]
        processed_header = [line.split() for line in processed_header]
        self.dimensions = self._extract_dimensions(processed_header)
        self.origin = self._extract_origin(processed_header)
        self.spacing = self._extract_spacing(processed_header)
        self.data_type = self._extract_data_type(processed_header)

    def _extract_dimensions(self, processed_header):
        assert(len(processed_header) == 10)
        dimensions = None
        for line in processed_header:
            if line[0] == "DIMENSIONS":
                dimensions = [int(float(value)) for value in line[1:4]]
                break
        return dimensions

    def _extract_origin(self, processed_header):
        assert(len(processed_header) == 10)
        origin = None
        for line in processed_header:
            if line[0] == "ORIGIN":
                origin = [int(float(value)) for value in line[1:4]]
                break
        return origin

    def _extract_spacing(self, processed_header):
        assert(len(processed_header) == 10)
        spacing = None
        for line in processed_header:
            if line[0] == "SPACING":
                spacing = [float(value) for value in line[1:4]]
                break
        return spacing

    def _extract_data_type(self, processed_header):
        assert(len(processed_header) == 10)
        data_type = None
        for line in processed_header:
            if line[0] == "SCALARS":
                data_type = line[2]
                break
        return data_type

    def _read_data(self):
        image_data = None
        number_of_header_lines = 10
        with open(self.filename, 'rb') as vtk_file:
            # Skip header lines
            for i in xrange(0, number_of_header_lines):
                vtk_file.readline()

            # Read the image data
            if self.data_type == "unsigned_char":
                image_data = numpy.fromfile(vtk_file, dtype=numpy.uint8)
            elif self.data_type == "unsigned_short":
                image_data = numpy.fromfile(vtk_file, dtype=numpy.uint16)
            elif self.data_type == "short":
                image_data = numpy.fromfile(vtk_file, dtype=numpy.int16)
            else:
                print("Warning: VTKReader does not support " + self.data_type +
                      " volumes.")
                sys.exit()

        # XXX: Swap byte order if necessary
        if self.data_type == "unsigned_short" or self.data_type == "short":
            max_value = numpy.max(image_data.flatten())
            if max_value == numpy.iinfo(image_data.dtype).max:
                image_data.byteswap(True)

        expected_number_of_elements = reduce(lambda x, y: x * y,
                                             self.dimensions)
        image_data = image_data[0:expected_number_of_elements]
        assert(len(image_data) == expected_number_of_elements)
        self.image_data = numpy.reshape(image_data, tuple(self.dimensions), order="F")


class VTKWriter(object):
    """Writes an uint8, uint16, or int16 volume image to a VTK file.

    Note: The input image_data array must be in Fortran (F) order!

    Attributes:
        filename: A string specifying the path to the VTK file to
            write.
        image_data: A MxNxP uint8, uint16, or int16 NumPy array
            containing the image data.
        dimensions: A 3-element list of integers specifying the
            dimensions (width,height,depth) of the image data.
        spacing: A 3-element list of floats specifying the voxel
            spacing (sx,sy,sz).
        origin: A 3-element list of integers specifying the origin
            (ox,oy,oz) of the image data.

    Usage:
    >>> writer = VTKWriter()
    >>> writer.filename = 'output.vtk'
    >>> writer.image_data = image_data
    >>> writer.dimensions = dimensions
    >>> writer.spacing = spacing
    >>> writer.origin = origin
    >>> writer.write()

    """

    def __init__(self):
        self.filename = ""
        self.image_data = None
        self.dimensions = [0, 0, 0]
        self.spacing = [1.0, 1.0, 1.0]
        self.origin = [0, 0, 0]

    def write(self):
        """Executes the writer."""
        if self.filename and self.image_data is not None:
            self._write_header()
            self._write_data()

    def _write_header(self):
        header = self._create_header()
        with open(self.filename, 'wb') as fid:
            fid.write(header)

    def _write_data(self):
        with open(self.filename, 'ab') as fid:
            self.image_data.ravel(order="F").tofile(fid)

    def _get_vtk_data_type(self):
        numpy_data_type = self.image_data.dtype.name
        vtk_data_type = None
        if numpy_data_type == "uint8":
            vtk_data_type = "unsigned_char"
        elif numpy_data_type == "uint16":
            vtk_data_type = "unsigned_short"
        elif numpy_data_type == "int16":
            vtk_data_type = "short"
        else:
            print("Warning: VTKWriter does not support " + numpy_data_type +
                  " volumes.")
            sys.exit()  # TODO: Implement proper error handling
        return vtk_data_type

    def _create_header(self):
        num_points = (self.dimensions[0] * self.dimensions[1] *
                      self.dimensions[2])
        header = ""
        header += "# vtk DataFile Version 3.0\n"
        header += "created by VTKWriter\n"
        header += "BINARY\n"
        header += "DATASET STRUCTURED_POINTS\n"
        header += "DIMENSIONS %d %d %d\n" % tuple(self.dimensions)
        header += "ORIGIN %d %d %d\n" % tuple(self.origin)
        header += "SPACING %f %f %f\n" % tuple(self.spacing)
        header += "POINT_DATA %d\n" % num_points
        header += "SCALARS image_data " + self._get_vtk_data_type() + "\n"
        header += "LOOKUP_TABLE default\n"
        return header
