import numpy
import pylab


def mix(x, y, alpha):
    return (1.0 - alpha) * y + alpha * x


def hg(mu, g=0.0):
    return 0.5 * (1.0 - g * g) / ((1.0 + g * g - 2.0 * g * mu) ** 1.5)


def main():
    num_points = 100
    angles = numpy.linspace(0.0, 2.0 * numpy.pi, num_points)
    x = numpy.cos(angles)
    y = numpy.sin(angles)

    cosine_weights = numpy.maximum(0.0, y)
    x_cosine = cosine_weights * x
    y_cosine = cosine_weights * y

    weights = 0.5 * y + 0.5
    weights = weights
    x_cosine2 = weights * x
    y_cosine2 = weights * y

    x_mixed = mix(x, x_cosine2, 0.5)
    y_mixed = mix(y, y_cosine2, 0.5)

    weights_hg = hg(y, 0.3)
    weights_hg = weights_hg / numpy.max(weights_hg)
    x_hg = weights_hg * x
    y_hg = weights_hg * y

    pylab.plot(x, y)
    pylab.plot(x_cosine, y_cosine)
    pylab.plot(x_cosine2, y_cosine2)
    pylab.plot(x_mixed, y_mixed)
    pylab.plot(x_hg, y_hg)
    pylab.axis("square")
    pylab.show()


if __name__ == "__main__":
    main()
