#!/bin/bash

export PBRVOL_ROOT=$(pwd)

# Generate build script
cd $PBRVOL_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$PBRVOL_ROOT ../ && \

# Build and install the program
make -j4 && \
make install && \

# Run the program
cd ../bin && \
./pbrvol
