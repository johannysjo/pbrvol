# PBRVol
PBRVol is a realtime volume renderer that combines physically-based shading, volumetric ambient occlusion, deep shadow maps, and HDR lighting to produce realistic visualizations of 3D CT scans (or other types of volume data.) The renderer is implemented in C++ and OpenGL 4.5 and features the following rendering techniques:

- GPU-accelerated volume ray casting
- Empty-space skipping
- Volumetric ambient occlusion (SPAO or voxel cone tracing)
- Deep shadow maps
- Physically-based shading
- HDR lighting (with bloom!)
- Filmic tone mapping
- FXAA

## Example screenshot
![Screenshot](https://bitbucket.org/johannysjo/pbrvol/raw/master/resources/screenshot.png "Screenshot")
## Compiling (Linux)
Clone the git repository and run (from the root folder)
```bash
$ ./build.sh
```
Requires CMake 3.1 or higher and a C++11 capable compiler (GCC 4.6+).
## Usage
Set the environment variable
```bash
$ export PBRVOL_ROOT=/path/to/pbrvol/directory
```
and run the program as
```bash
$ bin/pbrvol data/manix.vtk
```
Currently, the renderer can only load volume image files on the legacy VTK uint8 format. It also supports loading of HDR environment maps (32-bit .hdr images), which can be downloaded from [here](https://polyhaven.com/hdris). Volumes and HDR environment maps can be loaded into the program with drag and drop.
## License
PBRVol is provided under the MIT license. See LICENSE.txt for more information.
