cmake_minimum_required(VERSION 3.1)

project(PBRVOL)

if(UNIX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wshadow -pedantic -std=c++11 -fopenmp")
endif(UNIX)

aux_source_directory("${CMAKE_CURRENT_SOURCE_DIR}/src" PBRVOL_SRCS)
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")

set(requiredLibs)

# OpenGL
find_package(OpenGL REQUIRED)
if(OPENGL_FOUND)
  include_directories(SYSTEM ${OPENGL_INCLUDE_DIR})
  set(requiredLibs ${requiredLibs} ${OPENGL_LIBRARIES})
endif(OPENGL_FOUND)

# GLFW
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_INSTALL OFF CACHE BOOL "" FORCE)
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/ext/glfw" ${CMAKE_CURRENT_BINARY_DIR}/glfw)
include_directories(SYSTEM "${CMAKE_CURRENT_SOURCE_DIR}/ext/glfw/include")

# GLEW
aux_source_directory("${CMAKE_CURRENT_SOURCE_DIR}/ext/glew/src" PBRVOL_SRCS)
include_directories(SYSTEM "${CMAKE_CURRENT_SOURCE_DIR}/ext/glew/include")
add_definitions(-DGLEW_STATIC -DGLEW_NO_GLU)

# GLM
include_directories(SYSTEM "${CMAKE_CURRENT_SOURCE_DIR}/ext/glm")

# ImGui
aux_source_directory("${CMAKE_CURRENT_SOURCE_DIR}/ext/imgui" PBRVOL_SRCS)
include_directories(SYSTEM "${CMAKE_CURRENT_SOURCE_DIR}/ext/imgui")

# stb_image
include_directories(SYSTEM "${CMAKE_CURRENT_SOURCE_DIR}/ext/stb")

add_executable(pbrvol ${PBRVOL_SRCS})
target_link_libraries(pbrvol glfw ${requiredLibs} ${GLFW_LIBRARIES})
install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/pbrvol DESTINATION bin)
set(CMAKE_BUILD_TYPE RelWithDebInfo)
