#include "transfer_function.h"
#include "gfx.h"

#include <glm/exponential.hpp>

#include <cmath>
#include <stdint.h>

namespace {

float linearstep(const float edge0, const float edge1, const float value)
{
    return (std::fmax(edge0, std::fmin(edge1, value)) - edge0) / (edge1 - edge0);
}

glm::vec3 mix(const glm::vec3 &x, const glm::vec3 &y, const float a)
{
    return (1.0f - a) * x + a * y;
}

float mix(const float x, const float y, const float a)
{
    return (1.0f - a) * x + a * y;
}

glm::vec3 srgb2lin(const glm::vec3 &color)
{
    return glm::pow(color, glm::vec3(2.2f));
}

} // namespace

namespace pbrvol {

std::vector<TFPoint> get_default_transfer_function()
{
    std::vector<TFPoint> tf_points{};

    pbrvol::TFPoint tf_point1;
    tf_point1.position = 0.14f;
    tf_point1.material.albedo = glm::vec3(0.7f, 0.6f, 0.5f);
    tf_points.push_back(tf_point1);

    pbrvol::TFPoint tf_point2;
    tf_point2.position = 0.25f;
    tf_point2.material.albedo = glm::vec3(0.5f, 0.2f, 0.2f);
    tf_points.push_back(tf_point2);

    pbrvol::TFPoint tf_point3;
    tf_point3.position = 0.33f;
    tf_point3.material.albedo = glm::vec3(0.7f, 0.675f, 0.6f);
    tf_points.push_back(tf_point3);

    pbrvol::TFPoint tf_point4;
    tf_point4.position = 1.0f;
    tf_point4.material.albedo = glm::vec3(0.75f, 0.725f, 0.65f);
    tf_points.push_back(tf_point4);

    return tf_points;
}

// Creates a material transfer function texture from a set of TF points. Assumes that the TF points
// are sorted by position in increasing order.
void create_tf_texture(const std::vector<TFPoint> &tf_points, gfx::Texture2D &texture)
{
    assert(!tf_points.empty());

    const uint32_t num_tf_points = tf_points.size();

    const uint32_t width = 256;
    const uint32_t num_channels = 4;
    std::vector<uint8_t> tf(width * num_channels, 0);
    for (uint32_t i = 0; i < width; ++i) {
        // The intensity value calculated here corresponds to the normalized voxel intensity that is
        // used as TF texture coordinate in the GLSL shader
        const float intensity = static_cast<float>(i) / (width - 1);

        // Map intensity to a PBR material defined by the TF points
        Material material{};
        if (intensity <= tf_points[0].position) {
            material.albedo = srgb2lin(tf_points[0].material.albedo);
        }
        else if (intensity >= tf_points[num_tf_points - 1].position) {
            material.albedo = srgb2lin(tf_points[num_tf_points - 1].material.albedo);
        }
        else { // interpolate between two material TF points
            uint32_t index = 0;
            for (uint32_t j = 0; j < num_tf_points - 1; ++j) {
                if (intensity >= tf_points[j].position) {
                    index = j;
                }
            }

            const float mix_factor =
                linearstep(tf_points[index].position, tf_points[index + 1].position, intensity);
            material.albedo = mix(srgb2lin(tf_points[index].material.albedo),
                                  srgb2lin(tf_points[index + 1].material.albedo), mix_factor);
        }

        tf[i * 4] = static_cast<uint8_t>(255.99f * material.albedo[0]);
        tf[i * 4 + 1] = static_cast<uint8_t>(255.99f * material.albedo[1]);
        tf[i * 4 + 2] = static_cast<uint8_t>(255.99f * material.albedo[2]);
    }

    // Upload the material TF to the output texture
    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = 1;
    texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, tf.data());
}

// Creates an opacity transfer function texture from a set of opacity TF points. Assumes that the TF
// points are sorted by position in increasing order.
void create_opacity_tf_texture(const std::vector<OpacityTFPoint> &opacity_tf_points,
                               gfx::Texture2D &texture)
{
    assert(!opacity_tf_points.empty());

    const uint32_t num_tf_points = opacity_tf_points.size();

    const uint32_t width = 256;
    std::vector<uint8_t> opacity_tf{};
    opacity_tf.reserve(width);
    for (uint32_t i = 0; i < width; ++i) {
        // The intensity value calculated here corresponds to the normalized voxel intensity that is
        // used as TF texture coordinate in the GLSL shader
        const float intensity = static_cast<float>(i) / (width - 1);

        // Map intensity to an opacity value defined by the TF points
        float opacity = 0.0f;
        if (intensity <= opacity_tf_points[0].position) {
            opacity = opacity_tf_points[0].opacity;
        }
        else if (intensity >= opacity_tf_points[num_tf_points - 1].position) {
            opacity = opacity_tf_points[num_tf_points - 1].opacity;
        }
        else { // interpolate between two opacity TF points
            for (uint32_t j = 0; j < num_tf_points - 1; ++j) {
                if (intensity >= opacity_tf_points[j].position) {
                    opacity = mix(opacity_tf_points[j].opacity, opacity_tf_points[j + 1].opacity,
                                  linearstep(opacity_tf_points[j].position,
                                             opacity_tf_points[j + 1].position, intensity));
                    break;
                }
            }
        }

        opacity_tf.push_back(static_cast<uint8_t>(255.99f * opacity));
    }

    // Upload the opacity TF to the output texture
    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = 1;
    texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, opacity_tf.data());
}

} // namespace pbrvol
