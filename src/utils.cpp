#include "utils.h"
#include "gfx.h"

#include <glm/gtc/type_ptr.hpp>
#define STBI_ONLY_PNG
#define STBI_ONLY_HDR
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>

namespace pbrvol {

void fit_frustum_to_bsphere(const float bsphere_radius, const glm::vec3 &bsphere_center,
                            Camera &camera)
{
    assert(camera.fovy > 0.0f);
    assert(camera.fovy <= glm::pi<float>());
    const float theta = 0.5f * camera.fovy;
    const float eye_to_center_dist = camera.z_near + bsphere_radius / std::sin(theta);
    assert(glm::length(camera.center - camera.eye));
    const glm::vec3 view_dir = glm::normalize(camera.center - camera.eye);

    camera.eye = bsphere_center - view_dir * eye_to_center_dist;
    camera.center = bsphere_center;
    camera.z_far = eye_to_center_dist + 1.1f * bsphere_radius;
}

glm::vec3 srgb2lin(const glm::vec3 &color)
{
    return glm::pow(color, glm::vec3(2.2f));
}

gfx::Texture3D create_uint8_volume_texture(const std::vector<uint8_t> &voxel_data,
                                           const glm::uvec3 &dimensions)
{
    assert(dimensions[0] > 0);
    assert(dimensions[1] > 0);
    assert(dimensions[2] > 0);
    assert(voxel_data.size() == uint64_t(dimensions[0]) * dimensions[1] * dimensions[2]);

    auto texture = gfx::Texture3D{};
    texture.target = GL_TEXTURE_3D;
    texture.width = dimensions[0];
    texture.height = dimensions[1];
    texture.depth = dimensions[2];
    texture.max_level = 0;
    texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_BORDER};
    gfx::texture_3d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_3d_upload_data(texture, voxel_data.data());

    return texture;
}

gfx::Texture2D load_rgba8_texture(const std::string &filename)
{
    int32_t width, height, num_channels;
    uint8_t *data = stbi_load(filename.c_str(), &width, &height, &num_channels, 4);
    assert(data != nullptr);
    assert(width > 0);
    assert(height > 0);

    auto texture = gfx::Texture2D{};
    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);

    return texture;
}

gfx::Texture2D load_longlat_hdr_texture(const std::string &filename)
{
    int32_t width, height, num_channels;
    float *data = stbi_loadf(filename.c_str(), &width, &height, &num_channels, 4);
    assert(data != nullptr);
    assert(width > 0);
    assert(height > 0);

    auto texture = gfx::Texture2D{};
    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);

    return texture;
}

gfx::TextureCubemap resample_to_cubemap(const GLuint program, const gfx::Texture2D &longlat_envmap)
{
    const uint32_t width = 128;
    const uint32_t height = 128;

    // Allocate cubemap texture with mip levels
    auto cubemap_texture = gfx::TextureCubemap{};
    cubemap_texture.target = GL_TEXTURE_CUBE_MAP;
    cubemap_texture.width = width;
    cubemap_texture.height = height;
    assert(cubemap_texture.width == cubemap_texture.height);
    cubemap_texture.max_level = std::log2(cubemap_texture.width);
    cubemap_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
    cubemap_texture.sampling = {GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_cubemap_create(cubemap_texture);
    assert(cubemap_texture.texture != 0);

    // Resample the equirectangular environment map texture onto the six cubemap faces
    const std::vector<glm::mat4> rotations = {
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f))};

    const glm::ivec2 local_size = {8, 8};
    glBindTextureUnit(0, longlat_envmap.texture);
    for (uint32_t face_index = 0; face_index < rotations.size(); ++face_index) {
        glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &rotations[face_index][0][0]);
        glBindImageTexture(0, cubemap_texture.texture, 0, GL_FALSE, face_index, GL_WRITE_ONLY,
                           GL_RGBA16F);
        glUseProgram(program);
        glDispatchCompute(width / local_size[0] + 1, height / local_size[1] + 1, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }

    // Generate mip levels for approximate Blinn-Phong lobe prefiltering
    glGenerateTextureMipmap(cubemap_texture.texture);

    return cubemap_texture;
}

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename)
{
    std::string vshader_src;
    {
        std::FILE *fp = std::fopen(vertex_shader_filename.c_str(), "rb");
        assert(fp != nullptr);
        std::fseek(fp, 0, SEEK_END);
        vshader_src.resize(std::ftell(fp));
        assert(vshader_src.size() > 0);
        std::rewind(fp);
        const size_t num_chars = std::fread(&vshader_src[0], 1, vshader_src.size(), fp);
        assert(num_chars == vshader_src.size());
        std::fclose(fp);
    }

    std::string fshader_src;
    {
        std::FILE *fp = std::fopen(fragment_shader_filename.c_str(), "rb");
        assert(fp != nullptr);
        std::fseek(fp, 0, SEEK_END);
        fshader_src.resize(std::ftell(fp));
        assert(fshader_src.size() > 0);
        std::rewind(fp);
        const size_t num_chars = std::fread(&fshader_src[0], 1, fshader_src.size(), fp);
        assert(num_chars == fshader_src.size());
        std::fclose(fp);
    }

    const GLuint program = gfx::create_shader_program(vshader_src.c_str(), fshader_src.c_str());

    return program;
}

GLuint load_shader_program(const std::string &compute_shader_filename)
{
    std::FILE *fp = std::fopen(compute_shader_filename.c_str(), "rb");
    assert(fp != nullptr);
    std::fseek(fp, 0, SEEK_END);
    std::string cshader_src;
    cshader_src.resize(std::ftell(fp));
    assert(cshader_src.size() > 0);
    std::rewind(fp);
    const size_t num_chars = std::fread(&cshader_src[0], 1, cshader_src.size(), fp);
    assert(num_chars == cshader_src.size());
    std::fclose(fp);

    const GLuint program = gfx::create_shader_program(cshader_src.c_str());

    return program;
}

float compute_exposure(const float aperture, const float shutter_time, const float iso)
{
    const float ev100 = glm::log2(glm::pow(aperture, 2.0f) / shutter_time * 100.0f / iso);
    const float max_luminance = 1.2f * glm::pow(2.0f, ev100);

    return 1.0f / max_luminance;
}

GLuint create_quad_vao()
{
    // clang-format off
    const std::vector<glm::vec3> vertices = {
        {-1.0f, -1.0f, 0.0f},
        {1.0f, -1.0f, 0.0f},
        {1.0f,  1.0f, 0.0f},
        {-1.0f, -1.0f, 0.0f},
        {1.0f, 1.0f,  0.0f},
        {-1.0f, 1.0f, 0.0f}
    };
    // clang-format on

    GLuint vertex_buffer;
    glCreateBuffers(1, &vertex_buffer);
    glNamedBufferStorage(vertex_buffer, vertices.size() * sizeof(vertices[0]), vertices.data(),
                         GL_DYNAMIC_STORAGE_BIT);

    GLuint vao;
    glCreateVertexArrays(1, &vao);
    const uint32_t vertex_attrib_index = 0;
    glVertexArrayVertexBuffer(vao, vertex_attrib_index, vertex_buffer, 0, sizeof(vertices[0]));
    glEnableVertexArrayAttrib(vao, vertex_attrib_index);
    glVertexArrayAttribFormat(vao, vertex_attrib_index, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, vertex_attrib_index, vertex_attrib_index);

    return vao;
}

} // namespace pbrvol
