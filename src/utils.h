#pragma once

#include "gfx.h"

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <stdint.h>
#include <string>
#include <vector>

namespace pbrvol {

struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 color;
    float intensity;
};

struct Camera {
    glm::vec3 eye;
    glm::vec3 center;
    glm::vec3 up;
    float fovy;
    float aspect;
    float z_near;
    float z_far;
};

void fit_frustum_to_bsphere(float bsphere_radius, const glm::vec3 &bsphere_center, Camera &camera);

struct LightCamera {
    glm::mat4 view_matrix;
    glm::mat4 projection_matrix;
};

glm::vec3 srgb2lin(const glm::vec3 &color);

gfx::Texture3D create_uint8_volume_texture(const std::vector<uint8_t> &voxel_data,
                                           const glm::uvec3 &dimensions);

gfx::Texture2D load_rgba8_texture(const std::string &filename);

gfx::Texture2D load_longlat_hdr_texture(const std::string &filename);

gfx::TextureCubemap resample_to_cubemap(GLuint program, const gfx::Texture2D &longlat_envmap);

GLuint load_shader_program(const std::string &vertex_shader_filename,
                           const std::string &fragment_shader_filename);

GLuint load_shader_program(const std::string &compute_shader_filename);

float compute_exposure(float aperture, float shutter_time, float iso);

GLuint create_quad_vao();

} // namespace pbrvol
