#pragma once

#include "gfx.h"

#include <GL/glew.h>
#include <glm/vec3.hpp>

#include <string>
#include <unordered_map>

namespace pbrvol {

gfx::Texture3D downsample_volume(const std::unordered_map<std::string, GLuint> &programs,
                                 const gfx::Texture3D &src_volume_texture,
                                 const glm::vec3 &src_spacing);

gfx::Texture3D compute_ambient_occlusion(const std::unordered_map<std::string, GLuint> &programs,
                                         const gfx::Texture3D &volume_texture,
                                         const gfx::Texture2D &opacity_tf_texture,
                                         const glm::vec3 &spacing, float normal_factor,
                                         float radius_mm);

} // namespace pbrvol
