#version 450

in VS_OUT {
    vec4 color;
} fs_in;

layout(location = 0) out vec4 frag_color;

void main()
{
    frag_color = fs_in.color;
}
