// Implementation of the fast approximative anti-aliasing (FXAA) algorithm by
// Timothy Lottes. This version uses only four samples for edge detection and
// does not perform a full end-of-edge search, which will cause some artifacts
// and over-blurring but still produce acceptable results.
#version 450

layout(binding = 0) uniform sampler2D u_color_texture;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

float rgb2luma(vec3 color)
{
    return dot(vec3(0.299, 0.587, 0.114), color);
}

vec3 fxaa(sampler2D tex, vec2 texcoord)
{
    float subpix_shift = 0.25;
    vec4 pos = vec4(texcoord, texcoord - (0.5 + subpix_shift) / textureSize(tex, 0));

    // Sample neighborhood sRGB values and convert them to luma
    float luma_nw = rgb2luma(textureOffset(tex, pos.zw, ivec2(0, 0)).xyz);
    float luma_ne = rgb2luma(textureOffset(tex, pos.zw, ivec2(1, 0)).xyz);
    float luma_sw = rgb2luma(textureOffset(tex, pos.zw, ivec2(0, 1)).xyz);
    float luma_se = rgb2luma(textureOffset(tex, pos.zw, ivec2(1, 1)).xyz);
    float luma_m  = rgb2luma(texture(tex, pos.xy).xyz);
    float luma_min = min(luma_m, min(min(luma_nw, luma_ne), min(luma_sw, luma_se)));
    float luma_max = max(luma_m, max(max(luma_nw, luma_ne), max(luma_sw, luma_se)));

    // Find local edge direction
    vec2 dir = vec2(-((luma_nw + luma_ne) - (luma_sw + luma_se)),
        (luma_nw + luma_sw) - (luma_ne + luma_se));
    float reduce_min = 1.0 / 128.0;
    float reduce_mul = 1.0 / 8.0;
    float dir_reduce = max(reduce_min,
        (luma_nw + luma_ne + luma_sw + luma_se) * 0.25 * reduce_mul);
    float rcp_dir_min = 1.0 / (min(abs(dir.x), abs(dir.y)) + dir_reduce);
    vec2 span_max = vec2(8.0);
    dir = clamp(dir * rcp_dir_min, -span_max, span_max) / textureSize(tex, 0);

    // Blur along the edge to reduce aliasing
    vec3 rgb_a = 0.5 * (texture(tex, pos.xy - dir / 6.0).xyz +
        texture(tex, pos.xy + dir / 6.0).xyz);
    vec3 rgb_b = 0.5 * rgb_a + 0.25 * (texture(tex, pos.xy - dir / 2.0).xyz +
        texture(tex, pos.xy + dir / 2.0).xyz);
    float luma_b = rgb2luma(rgb_b);
    vec3 output_color = (luma_b < luma_min) || (luma_b > luma_max) ? rgb_a : rgb_b;

    return output_color;
}

void main()
{
    frag_color = vec4(fxaa(u_color_texture, fs_in.texcoord), 1.0);
}
