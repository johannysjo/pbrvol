#version 450

layout(location = 0) uniform mat4 u_mvp;

layout(location = 0) in vec4 a_position;

out VS_OUT {
    vec2 texcoord;
} vs_out;

void main()
{
    vs_out.texcoord = 0.5 * a_position.xy + 0.5;
    gl_Position = u_mvp * a_position;
}
