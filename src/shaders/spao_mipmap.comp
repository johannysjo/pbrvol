#version 450

layout(local_size_x = 4, local_size_y = 4, local_size_z = 4) in;

layout(binding = 0, r8) uniform restrict readonly image3D u_src_image; // mipmap level i
layout(binding = 1, r8) uniform restrict writeonly image3D u_dst_image; // mipmap level i + 1

void main()
{
    // Calculate the voxel value for the current mipmap level i + 1 by averaging 2x2x2 voxels from
    // the previous mipmap level i.
    ivec3 dst_imcoord = ivec3(gl_GlobalInvocationID.xyz);
    float dst_value = 0.0;
    for (int k = 0; k < 2; ++k) {
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                ivec3 src_imcoord = 2 * dst_imcoord + ivec3(i, j, k);
                dst_value += imageLoad(u_src_image, src_imcoord).r;
            }
        }
    }
    dst_value /= 8.0;

    imageStore(u_dst_image, dst_imcoord, vec4(dst_value));
}
