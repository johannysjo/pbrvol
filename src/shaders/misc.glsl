float diffuse_oren_nayar(in vec3 L, in vec3 N, float roughness)
{
    float rho = max(0.001, 1.6 * roughness);
    float normalization = 1.0 / (-0.0427 * rho * rho + 0.1625 * rho + 1.002);
    float k = (1.0 + rho) / rho * normalization;
    return k * (1.0 - 1.0 / (1.0 + rho * max(0.0, dot(L, N))));
}

vec3 fresnel_schlick(in vec3 R_F0, in vec3 E, in vec3 H)
{
    return R_F0 + (1.0 - R_F0) * pow(1.0 - max(0.0, dot(E, H)), 5.0);
}

vec3 fresnel_schlick_sg(in vec3 R_F0, in vec3 E, in vec3 H)
{
    float E_dot_H = max(0.0, dot(E, H));
    return R_F0 + (1.0 - R_F0) * exp2((-5.55473 * E_dot_H - 6.98316) * E_dot_H);
}

vec3 fresnel_schlick_with_roughness_sg(in vec3 R_F0, in vec3 E, in vec3 N, in float gloss)
{
    float E_dot_N = max(0.0, dot(E, N));
    return R_F0 + (max(vec3(gloss), R_F0) - R_F0) * exp2((-5.55473 * E_dot_N - 6.98316) * E_dot_N);
}

// Computes the closest representative point (CRP) on a spherical
// light source. Works well for specular but not diffuse.
//
// Reference: Brian Karnis, "Real Shading in Unreal Engine 4",
// SIGGRAPH 2013
vec3 sphere_light_CRP(in vec3 N, in vec3 R, in vec3 eye, in vec3 light_center, in float light_radius)
{
    vec3 L = (light_center - eye);
    vec3 center_to_ray = dot(L, R) * R - L;
    vec3 closest_point = L + center_to_ray * clamp(light_radius / length(center_to_ray), 0.0, 1.0);
    return closest_point;
}

// Computes sphere light normalization factor for Blinn-Phong
float sphere_normalization_blinn_phong(float light_radius, in vec3 closest_point, float gloss)
{
	// Use the solid angle to the sphere light source to estimate a new gloss
    // value that widens the highlight. Based on the specular D modification
    // proposed by Brian Karnis, "Real Shading in Unreal Engine 4", SIGGRAPH 2013.
    float d = length(closest_point);
    float roughness = 1.0 - gloss;
    float alpha = roughness * roughness;
    float alpha_new = clamp(alpha + 0.5 * light_radius / d, 0.0, 1.0);
    float gloss_new = 1.0 - sqrt(alpha_new);

    // Compute sphere normalization factor
    float specular_power = pow(2.0, 10.0 * gloss + 1.0);
    float specular_power_new = pow(2.0, 10.0 * gloss_new + 1.0);
    float normalization_factor = (8.0 + specular_power_new) / (8.0 + specular_power);

    return normalization_factor;
}

float interpolate_tricubic(in sampler3D volume, in vec3 texcoord)
{
    int lod = 0;

    vec3 texture_size = vec3(textureSize(volume, lod));
    vec3 grid_coord = texcoord * texture_size - 0.5;
    vec3 index = floor(grid_coord);
    vec3 u = fract(grid_coord);
    vec3 u2 = u * u;
    vec3 u3 = u * u2;

    // Compute filter weights
    vec3 w0 = (-u3 + 3 * u2 - 3 * u + 1.0) / 6.0;
    vec3 w1 = (3.0 * u3 - 6 * u2 + 4.0) / 6.0;
    vec3 w2 = (-3.0 * u3 + 3.0 * u2 + 3.0 * u + 1.0) / 6.0;
    vec3 w3 = u3 / 6.0;

    // Adjust sampling coordinates
    vec3 g0 = w0 + w1;
    vec3 g1 = w2 + w3;
    vec3 h0 = ((w1 / g0) - 0.5 + index) / texture_size;
    vec3 h1 = ((w3 / g1) + 1.5 + index) / texture_size;

    // Perform tricubic interpolation with eight trilinear texture lookups
    float sample000 = textureLod(volume, h0, lod).r;
    float sample100 = textureLod(volume, vec3(h1.x, h0.y, h0.z), lod).r;
    sample000 = mix(sample100, sample000, g0.x);
    float sample010 = textureLod(volume, vec3(h0.x, h1.y, h0.z), lod).r;
    float sample110 = textureLod(volume, vec3(h1.x, h1.y, h0.z), lod).r;
    sample010 = mix(sample110, sample010, g0.x);
    sample000 = mix(sample010, sample000, g0.y);
    float sample001 = textureLod(volume, vec3(h0.x, h0.y, h1.z), lod).r;
    float sample101 = textureLod(volume, vec3(h1.x, h0.y, h1.z), lod).r;
    sample001 = mix(sample101, sample001, g0.x);
    float sample011 = textureLod(volume, vec3(h0.x, h1.y, h1.z), lod).r;
    float sample111 = textureLod(volume, h1, lod).r;
    sample011 = mix(sample111, sample011, g0.x);
    sample001 = mix(sample011, sample001, g0.y);

    return mix(sample001, sample000, g0.z);
}
