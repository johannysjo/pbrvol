#version 450

layout(binding = 0) uniform sampler2D u_tf_texture;

in VS_OUT {
    vec2 texcoord;
} fs_in;

layout(location = 0) out vec4 frag_color;

struct Material {
    vec3 base_color;
};

Material transfer_function(float intensity)
{
    Material material;
    material.base_color = texture(u_tf_texture, vec2(intensity, 0.5)).rgb;

    return material;
}

vec3 lin2srgb(vec3 color)
{
    return pow(color, vec3(0.454));
}

void main()
{
    Material material = transfer_function(fs_in.texcoord.x);
    frag_color = vec4(lin2srgb(material.base_color), 1.0);
}
