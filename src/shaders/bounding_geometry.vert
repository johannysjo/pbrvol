#version 450

layout(location = 0) uniform mat4 u_view_from_model;
layout(location = 1) uniform mat4 u_proj_from_view;

layout(location = 0) in vec4 a_position;

out VS_OUT {
    vec4 color;
} vs_out;

void main()
{
    vs_out.color = vec4(0.5 * a_position.xyz + 0.5, 1.0);
    gl_Position = u_proj_from_view * u_view_from_model * a_position;
}
