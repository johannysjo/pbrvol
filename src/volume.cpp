#include "volume.h"

#include <glm/gtc/matrix_transform.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <omp.h>

namespace {

void byteswap(std::vector<int16_t> &values)
{
    const size_t num_values = values.size();
#pragma omp parallel for schedule(static)
    for (size_t i = 0; i < num_values; ++i) {
        const int16_t x = values[i];
        values[i] = ((x << 8) & 0xFF00) | ((x >> 8) & 0x00FF);
    }
}

// Converts int16 Hounsfield unit (HU) values to normalized uint8 values in the range [0,255]
void int16_to_uint8(const std::vector<int16_t> &int16_hu_values, std::vector<uint8_t> &uint8_values)
{
    const double min_hu = -1024.0;
    const double max_hu = 3071.0;

    const uint64_t num_values = int16_hu_values.size();
    uint8_values.resize(num_values);
#pragma omp parallel for schedule(static)
    for (uint64_t i = 0; i < num_values; ++i) {
        const double hu_clamped = std::fmax(min_hu, std::fmin(max_hu, double(int16_hu_values[i])));
        const double value_01 = (hu_clamped - min_hu) / (max_hu - min_hu);
        uint8_values[i] = uint8_t(std::fmin(255.0, std::floor(256.0 * value_01)));
    }
}

} // namespace

namespace pbrvol {

bool load_uint8_vtk_volume(const char *filename, VolumeUInt8 &volume)
{
    std::FILE *fp = std::fopen(filename, "rb");
    if (fp == nullptr) {
        std::fclose(fp);
        return false;
    }

    // Read header
    const uint32_t num_header_lines = 10;
    const uint32_t max_line_length = 256;
    char lines[num_header_lines][max_line_length];
    for (uint32_t i = 0; i < num_header_lines; ++i) {
        if (std::fgets(lines[i], max_line_length, fp) == nullptr) {
            std::fclose(fp);
            return false;
        }
    }

    if (!(std::strncmp(lines[0], "# vtk", 5) == 0 || std::strncmp(lines[0], "# VTK", 5) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[2], "BINARY", 6) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[3], "DATASET STRUCTURED_POINTS", 25) == 0)) {
        std::fclose(fp);
        return false;
    }
    uint32_t dx, dy, dz = 0;
    if (!(std::strncmp(lines[4], "DIMENSIONS", 10) == 0 &&
          std::sscanf(lines[4], "%*s %u %u %u", &dx, &dy, &dz) == 3)) {
        std::fclose(fp);
        return false;
    }
    float sx, sy, sz = 0.0f;
    // spacing and origin might be swapped, so we need to check both lines 5 and 6
    if (!((std::strncmp(lines[5], "SPACING", 7) == 0 &&
           std::sscanf(lines[5], "%*s %f %f %f", &sx, &sy, &sz) == 3) ||
          (std::strncmp(lines[6], "SPACING", 7) == 0 &&
           std::sscanf(lines[6], "%*s %f %f %f", &sx, &sy, &sz) == 3))) {
        std::fclose(fp);
        return false;
    }
    size_t num_voxels = 0;
    if (!(std::strncmp(lines[7], "POINT_DATA", 10) == 0 &&
          std::sscanf(lines[7], "%*s %lu", &num_voxels) == 1)) {
        std::fclose(fp);
        return false;
    }
    assert(size_t(dx) * dy * dz == num_voxels);
    char data_type[256];
    if (!(std::strncmp(lines[8], "SCALARS", 7) == 0 &&
          std::sscanf(lines[8], "%*s %*s %s", data_type) == 1)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(data_type, "unsigned_char", 13) == 0 ||
          std::strncmp(data_type, "short", 5) == 0)) {
        std::fclose(fp);
        return false;
    }

    volume.dimensions = glm::uvec3(dx, dy, dz);
    volume.spacing = glm::vec3(sx, sy, sz);

    // Read data
    volume.data.resize(num_voxels);
    if (std::strncmp(data_type, "unsigned_char", 13) == 0) {
        const size_t result = std::fread(volume.data.data(), sizeof(uint8_t), num_voxels, fp);
        std::fclose(fp);
        if (result != num_voxels) {
            return false;
        }
    }
    else if (std::strncmp(data_type, "short", 5) == 0) {
        std::vector<int16_t> int16_volume_data(num_voxels, 0);
        const size_t result = std::fread(int16_volume_data.data(), sizeof(int16_t), num_voxels, fp);
        std::fclose(fp);
        if (result != num_voxels) {
            return false;
        }
        byteswap(int16_volume_data);
        int16_to_uint8(int16_volume_data, volume.data);
    }

    return true;
}

glm::vec3 get_volume_extent(const VolumeUInt8 &volume)
{
    return glm::vec3(volume.dimensions) * volume.spacing;
}

// Returns a 4x4 transformation matrix that scales a 2-unit cube centered at origin to the physical
// extent of the volume image
glm::mat4 get_volume_matrix(const VolumeUInt8 &volume)
{
    return glm::scale(glm::mat4(1.0f), 0.5f * get_volume_extent(volume));
}

} // namespace pbrvol
