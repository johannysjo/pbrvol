#pragma once

#include <GL/glew.h>
#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace pbrvol {

struct VolumeUInt8; // forward declaration

void create_max_blocks(const VolumeUInt8 &volume, const glm::uvec3 &num_blocks,
                       VolumeUInt8 &max_blocks);

void create_block_volume_mesh(const glm::uvec3 &num_blocks, std::vector<glm::vec3> &vertices,
                              std::vector<uint32_t> &indices);

void get_non_empty_block_mesh_indices(const VolumeUInt8 &max_blocks,
                                      const std::vector<uint32_t> &indices, uint32_t threshold,
                                      std::vector<uint32_t> &non_empty_indices);

struct BoundingGeometryVao {
    GLuint vao;
    GLuint vertex_vbo;
    GLuint index_vbo;
    int32_t num_indices;
};

BoundingGeometryVao create_bounding_geometry(const VolumeUInt8 &max_block_volume,
                                             uint32_t threshold);

void delete_bounding_geometry(BoundingGeometryVao &bounding_geometry);

} // namespace pbrvol