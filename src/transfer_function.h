#pragma once

#include <glm/vec3.hpp>

#include <vector>

namespace gfx { // forward declarations
struct Texture2D;
} // namespace gfx

namespace pbrvol {

struct Material {
    glm::vec3 albedo;
};

struct TFPoint {
    float position;
    Material material;
};

struct OpacityTFPoint {
    float position;
    float opacity;
};

std::vector<TFPoint> get_default_transfer_function();

void create_tf_texture(const std::vector<TFPoint> &tf_points, gfx::Texture2D &texture);

void create_opacity_tf_texture(const std::vector<OpacityTFPoint> &opacity_tf_points,
                               gfx::Texture2D &texture);

} // namespace pbrvol
