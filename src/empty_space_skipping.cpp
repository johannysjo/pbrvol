#include "empty_space_skipping.h"
#include "volume.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <limits>
#include <omp.h>

namespace {

inline int64_t clamp(const int64_t x, const int64_t min_val, const int64_t max_val)
{
    assert(min_val <= max_val);
    return x < min_val ? min_val : (x > max_val ? max_val : x);
}

} // namespace

namespace pbrvol {

void create_max_blocks(const VolumeUInt8 &volume, const glm::uvec3 &num_blocks,
                       VolumeUInt8 &max_blocks)
{
    assert(volume.dimensions[0] > 0);
    assert(volume.dimensions[1] > 0);
    assert(volume.dimensions[2] > 0);
    assert(num_blocks[0] > 0);
    assert(num_blocks[1] > 0);
    assert(num_blocks[2] > 0);

    const size_t width = volume.dimensions[0];
    const size_t height = volume.dimensions[1];
    const size_t depth = volume.dimensions[2];
    assert(width * height * depth == volume.data.size());

    const double block_width = double(width) / num_blocks[0];
    const double block_height = double(height) / num_blocks[1];
    const double block_depth = double(depth) / num_blocks[2];

    // The high-resolution source volume will typically be sampled with some form of interpolation
    // during rendering, so we set a small (one-voxel thick) margin for the max block computation to
    // avoid artifacts at block boundaries
    const int64_t margin = 1;

    max_blocks.dimensions = num_blocks;
    max_blocks.spacing = glm::vec3(block_width, block_height, block_depth) * volume.spacing;
    max_blocks.data.resize(num_blocks[0] * num_blocks[1] * num_blocks[2]);

#pragma omp parallel for schedule(static)
    for (size_t k = 0; k < num_blocks[2]; ++k) {
        for (size_t j = 0; j < num_blocks[1]; ++j) {
            for (size_t i = 0; i < num_blocks[0]; ++i) {
                // Determine block bounds
                const size_t ii_min =
                    clamp(int64_t(std::floor(i * block_width)) - margin, 0, width - 1);
                const size_t ii_max =
                    clamp(int64_t(std::ceil((i + 1) * block_width)) + margin, 0, width);
                const size_t jj_min =
                    clamp(int64_t(std::floor(j * block_height)) - margin, 0, height - 1);
                const size_t jj_max =
                    clamp(int64_t(std::ceil((j + 1) * block_height)) + margin, 0, height);
                const size_t kk_min =
                    clamp(int64_t(std::floor(k * block_depth)) - margin, 0, depth - 1);
                const size_t kk_max =
                    clamp(int64_t(std::ceil((k + 1) * block_depth)) + margin, 0, depth);

                // Find maximum intensity value in block
                uint8_t max_intensity = 0;
                for (size_t kk = kk_min; kk < kk_max; ++kk) {
                    for (size_t jj = jj_min; jj < jj_max; ++jj) {
                        for (size_t ii = ii_min; ii < ii_max; ++ii) {
                            const size_t voxel_index = ii + jj * width + kk * width * height;
                            const uint8_t intensity = volume.data[voxel_index];
                            max_intensity = intensity > max_intensity ? intensity : max_intensity;
                        }
                    }
                }

                const size_t block_index =
                    i + j * num_blocks[0] + k * num_blocks[0] * num_blocks[1];
                max_blocks.data[block_index] = max_intensity;
            }
        }
    }
}

void create_block_volume_mesh(const glm::uvec3 &num_blocks, std::vector<glm::vec3> &vertices,
                              std::vector<uint32_t> &indices)
{
    assert(num_blocks[0] > 0);
    assert(num_blocks[1] > 0);
    assert(num_blocks[2] > 0);
    assert(vertices.empty());
    assert(indices.empty());

    // clang-format off
    std::vector<glm::vec3> unit_cube_vertices = {
        {0.0f, 0.0f, 0.0f},
        {1.0f, 0.0f, 0.0f},
        {1.0f, 1.0f, 0.0f},
        {0.0f, 1.0f, 0.0f},
        {0.0f, 0.0f, 1.0f},
        {1.0f, 0.0f, 1.0f},
        {1.0f, 1.0f, 1.0f},
        {0.0f, 1.0f, 1.0f}
    };

    std::vector<uint32_t> unit_cube_indices = {
        // front
        4, 5, 6,
        4, 6, 7,
        // right
        5, 1, 2,
        5, 2, 6,
        // back
        1, 0, 3,
        1, 3, 2,
        // left
        0, 4, 7,
        0, 7, 3,
        // bottom
        0, 1, 5,
        0, 5, 4,
        // top
        7, 6, 2,
        7, 2, 3
    };
    // clang-format on

    const uint64_t total_num_blocks = num_blocks[0] * num_blocks[1] * uint64_t(num_blocks[2]);
    vertices.reserve(total_num_blocks * unit_cube_vertices.size());
    indices.reserve(total_num_blocks * unit_cube_indices.size());
    assert(vertices.size() < std::numeric_limits<uint32_t>::max());

    const auto block_size = 1.0f / glm::vec3(num_blocks);
    uint32_t index_offset = 0;
    for (uint32_t k = 0; k < num_blocks[2]; ++k) {
        for (uint32_t j = 0; j < num_blocks[1]; ++j) {
            for (uint32_t i = 0; i < num_blocks[0]; ++i) {
                // Generate vertices for block (i,j,k)
                const glm::vec3 block_offset = glm::vec3(i, j, k) * block_size;
                for (const auto &vertex : unit_cube_vertices) {
                    vertices.push_back(2.0f * (vertex * block_size + block_offset) - 1.0f);
                }

                // Generate indices for block (i,j,k)
                for (const auto &index : unit_cube_indices) {
                    indices.push_back(index + index_offset);
                }

                index_offset += unit_cube_vertices.size();
            }
        }
    }
}

void get_non_empty_block_mesh_indices(const VolumeUInt8 &max_blocks,
                                      const std::vector<uint32_t> &indices,
                                      const uint32_t threshold,
                                      std::vector<uint32_t> &non_empty_indices)
{
    assert(non_empty_indices.empty());

    const glm::uvec3 num_blocks = max_blocks.dimensions;
    const uint64_t total_num_blocks = num_blocks[0] * num_blocks[1] * uint64_t(num_blocks[2]);
    const uint64_t num_indices_per_block = 36;
    assert(total_num_blocks * num_indices_per_block == indices.size());

    non_empty_indices.reserve(indices.size());
    for (uint64_t k = 0; k < num_blocks[2]; ++k) {
        for (uint64_t j = 0; j < num_blocks[1]; ++j) {
            for (uint64_t i = 0; i < num_blocks[0]; ++i) {
                const uint64_t block_index =
                    i + j * num_blocks[0] + k * num_blocks[0] * num_blocks[1];
                if (max_blocks.data[block_index] < threshold)
                    continue;

                for (uint64_t ii = 0; ii < num_indices_per_block; ++ii) {
                    non_empty_indices.push_back(indices[block_index * num_indices_per_block + ii]);
                }
            }
        }
    }
}

BoundingGeometryVao create_bounding_geometry(const VolumeUInt8 &max_block_volume,
                                             const uint32_t threshold)
{
    std::vector<glm::vec3> vertices{};
    std::vector<uint32_t> indices{};
    pbrvol::create_block_volume_mesh(max_block_volume.dimensions, vertices, indices);

    std::vector<uint32_t> non_empty_indices{};
    pbrvol::get_non_empty_block_mesh_indices(max_block_volume, indices, threshold,
                                             non_empty_indices);

    GLuint vertex_buffer;
    glCreateBuffers(1, &vertex_buffer);
    glNamedBufferStorage(vertex_buffer, vertices.size() * sizeof(vertices[0]), vertices.data(),
                         GL_DYNAMIC_STORAGE_BIT);

    GLuint index_buffer;
    glCreateBuffers(1, &index_buffer);
    glNamedBufferStorage(index_buffer, non_empty_indices.size() * sizeof(non_empty_indices[0]),
                         non_empty_indices.data(), GL_DYNAMIC_STORAGE_BIT);

    GLuint vao;
    glCreateVertexArrays(1, &vao);
    const uint32_t vertex_attrib_index = 0;
    glVertexArrayVertexBuffer(vao, vertex_attrib_index, vertex_buffer, 0, sizeof(vertices[0]));
    glEnableVertexArrayAttrib(vao, vertex_attrib_index);
    glVertexArrayAttribFormat(vao, vertex_attrib_index, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(vao, vertex_attrib_index, vertex_attrib_index);
    glVertexArrayElementBuffer(vao, index_buffer);

    BoundingGeometryVao bounding_geometry;
    bounding_geometry.vao = vao;
    bounding_geometry.vertex_vbo = vertex_buffer;
    bounding_geometry.index_vbo = index_buffer;
    bounding_geometry.num_indices = non_empty_indices.size();

    return bounding_geometry;
}

void delete_bounding_geometry(BoundingGeometryVao &bounding_geometry)
{
    glDeleteBuffers(1, &bounding_geometry.vertex_vbo);
    glDeleteBuffers(1, &bounding_geometry.index_vbo);
    glDeleteVertexArrays(1, &bounding_geometry.vao);
    bounding_geometry.num_indices = 0;
}

} // namespace pbrvol