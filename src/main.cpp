/// @file
/// @brief Realtime volume renderer that implements physically-based shading, volumetric ambient
/// occlusion, and deep shadow maps.
///
/// @section LICENSE
///
/// Copyright (C) 2015-2021  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "ao.h"
#include "empty_space_skipping.h"
#include "gfx.h"
#include "gfx_utils.h"
#include "logging.h"
#include "transfer_function.h"
#include "utils.h"
#include "volume.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

enum DisplayMode { DISPLAY_MODE_RAY_CASTING, DISPLAY_MODE_FRONT_FACES, DISPLAY_MODE_BACK_FACES };

enum ToneMappingOp {
    TONE_MAPPING_OP_ACES,
    TONE_MAPPING_OP_UNREAL,
    TONE_MAPPING_OP_GAMMA_ONLY,
    TONE_MAPPING_OP_NONE
};

enum BackgroundMode { BACKGROUND_MODE_ENVMAP, BACKGROUND_MODE_SOLID_COLOR };

enum TimeStamp {
    TIME_STAMP_BOUNDING_GEOMETRY,
    TIME_STAMP_BOUNDING_GEOMETRY_DSM,
    TIME_STAMP_DSM,
    TIME_STAMP_BACKGROUND,
    TIME_STAMP_RAY_CASTING,
    TIME_STAMP_BLOOM,
    TIME_STAMP_TONE_MAPPING,
    TIME_STAMP_FXAA,
    TIME_STAMP_TOTAL,
    NUM_TIME_STAMPS
};

struct SharedTransforms {
    glm::mat4 view_from_world;
    glm::mat4 proj_from_view;
    glm::mat4 key_light_view_from_world;
    glm::mat4 key_light_proj_from_view;
};

struct Window {
    GLFWwindow *handle = nullptr;
    int32_t width = 800;
    int32_t height = 600;
    uint64_t frame_index = 1;
};

struct AmbientOcclusionSettings {
    bool enabled = true;
    float radius_mm = 20.0f;
    float normal_factor = 20.0f;
};

struct DeepShadowMapSettings {
    bool enabled = true;
    uint32_t width = 512;
    uint32_t height = 512;
    uint32_t num_layers = 8;
    float step_size_voxels = 1.0f;
    float alpha_error = 0.125f;
    float bias = 0.015f;
};

struct BloomSettings {
    bool enabled = true;
    uint32_t downsampling = 4; // must be set to 4
    uint32_t num_iterations = 3;
    float intensity = 0.05f;
};

struct BackgroundSettings {
    BackgroundMode mode = BACKGROUND_MODE_SOLID_COLOR;
    glm::vec3 color = {0.5f, 0.5f, 0.5f};
    float intensity = 0.0f;
};

struct CameraSettings {
    float aperture = 12.0f;
    float shutter_time = 1.0f / 100.0f;
    float iso = 100.0f;
    float exposure_compensation = 0.0f;
};

struct Settings {
    DisplayMode display_mode = DISPLAY_MODE_RAY_CASTING;
    float step_length_voxels = 1.0f;
    bool jittering_enabled = true;
    float tf_alpha1 = 0.25f;
    float tf_alpha2 = 0.28f;
    bool display_transfer_function = false;
    float gloss = 0.75f;
    float wrap = 0.15f;
    float scatter_factor = 1.2f;
    AmbientOcclusionSettings ao;
    DeepShadowMapSettings dsm;
    float ambient_light_intensity = 35000.0f;
    BackgroundSettings background;
    BloomSettings bloom;
    ToneMappingOp tone_mapping_op = TONE_MAPPING_OP_ACES;
    bool fxaa_enabled = true;
    CameraSettings camera;
    float exposure = 1.0f;
};

struct UISettings {
    bool show_settings_ui = true;
    ImVec2 settings_ui_size = {260, 590};
    ImVec2 settings_ui_pos = {5, 5};
    bool settings_ui_collapsed = false;

    bool show_profiler_ui = true;
    ImVec2 profiler_ui_size = {260, 240};
    ImVec2 profiler_ui_pos = {settings_ui_size.x + settings_ui_pos.x + 5, 5};
    bool profiler_ui_collapsed = true;

    bool show_tf_ui = true;
    ImVec2 tf_ui_size = {260, 590};
    ImVec2 tf_ui_pos = {profiler_ui_size.x + profiler_ui_pos.x + 5, 5};
    bool tf_ui_collapsed = true;
};

struct Context {
    Window window;
    GLuint timer_queries_tic[NUM_TIME_STAMPS];
    GLuint timer_queries_toc[NUM_TIME_STAMPS];
    float frame_times_ms[NUM_TIME_STAMPS];

    GLuint default_vao = 0;
    GLuint quad_vao = 0;
    std::unordered_map<std::string, GLuint> programs{};
    std::unordered_map<std::string, gfx::FBO> fbos;

    pbrvol::Camera camera;
    pbrvol::LightCamera key_light_camera;
    gfx::Trackball trackball;
    SharedTransforms shared_transforms;

    std::string volume_filename = "";
    pbrvol::VolumeUInt8 volume;
    glm::mat4 volume_world_from_model;
    pbrvol::VolumeUInt8 max_block_volume;
    pbrvol::BoundingGeometryVao bounding_geometry;
    std::vector<pbrvol::TFPoint> tf_points;
    pbrvol::DirectionalLight key_light;

    gfx::Texture3D volume_texture;
    gfx::Texture3D isotropic_halfres_volume_texture;
    gfx::Texture3D ao_texture;
    gfx::Texture2DArray dsm_texture;
    gfx::Texture2D jitter_texture;
    gfx::Texture2D tf_texture;
    gfx::Texture2D opacity_tf_texture;
    gfx::Texture2D longlat_envmap_texture;
    gfx::TextureCubemap cubemap_texture;

    bool bounding_geometry_is_dirty = true;
    bool isotropic_halfres_volume_texture_is_dirty = true;
    bool ao_is_dirty = true;
    bool tf_texture_is_dirty = true;
    bool opacity_tf_texture_is_dirty = true;
    bool cubemap_texture_is_dirty = true;

    Settings settings;
    UISettings ui;
};

std::string root_dir()
{
    const char *path = std::getenv("PBRVOL_ROOT");
    const auto path_str = path != nullptr ? std::string(path) : std::string();
    if (path_str.empty()) {
        LOG_ERROR("PBRVOL_ROOT is not set.\n");
        std::exit(EXIT_FAILURE);
    }

    return path_str;
}

void load_shader_programs(Context &ctx)
{
    const std::string shader_dir = root_dir() + "/src/shaders/";

    // clang-format off
    ctx.programs["bounding_geometry"] = pbrvol::load_shader_program(
        shader_dir + "bounding_geometry.vert",
        shader_dir + "bounding_geometry.frag");

    ctx.programs["deep_shadow_map"] = pbrvol::load_shader_program(
        shader_dir + "deep_shadow_map.comp");

    ctx.programs["spao_downsample"] = pbrvol::load_shader_program(
        shader_dir + "spao_downsample.comp");

    ctx.programs["spao_classify"] = pbrvol::load_shader_program(
        shader_dir + "spao_classify.comp");

    ctx.programs["spao_mipmap"] = pbrvol::load_shader_program(
        shader_dir + "spao_mipmap.comp");

    ctx.programs["spao_ao"] = pbrvol::load_shader_program(
        shader_dir + "spao_ao.comp");

    ctx.programs["background"] = pbrvol::load_shader_program(
        shader_dir + "background.comp");

    ctx.programs["ray_caster"] = pbrvol::load_shader_program(
        shader_dir + "ray_caster.comp");

    ctx.programs["bloom_brightpass"] = pbrvol::load_shader_program(
        shader_dir + "bloom_brightpass.comp");

    ctx.programs["gaussian_blur"] = pbrvol::load_shader_program(
        shader_dir + "gaussian_blur.comp");

    ctx.programs["bloom_blend"] = pbrvol::load_shader_program(
        shader_dir + "bloom_blend.comp");

    ctx.programs["tone_mapping"] = pbrvol::load_shader_program(
        shader_dir + "tone_mapping.comp");

    ctx.programs["fxaa"] = pbrvol::load_shader_program(
        shader_dir + "screen_aligned_triangle.vert",
        shader_dir + "fxaa.frag");

    ctx.programs["transfer_function"] = pbrvol::load_shader_program(
        shader_dir + "transfer_function.vert",
        shader_dir + "transfer_function.frag");

    ctx.programs["longlat_to_cubemap"] = pbrvol::load_shader_program(
        shader_dir + "longlat_to_cubemap.comp");
    // clang-format on
}

void init_fbos(Context &ctx)
{
    { // bounding geometry
        auto frontface_texture = gfx::Texture2D{};
        frontface_texture.target = GL_TEXTURE_2D;
        frontface_texture.width = ctx.window.width;
        frontface_texture.height = ctx.window.height;
        frontface_texture.storage = {GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT, 1};
        frontface_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(frontface_texture);
        assert(frontface_texture.texture != 0);

        auto backface_texture = gfx::Texture2D{};
        backface_texture.target = GL_TEXTURE_2D;
        backface_texture.width = ctx.window.width;
        backface_texture.height = ctx.window.height;
        backface_texture.storage = {GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT, 1};
        backface_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(backface_texture);
        assert(backface_texture.texture != 0);

        auto depth_texture = gfx::Texture2D{};
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = ctx.window.width;
        depth_texture.height = ctx.window.height;
        depth_texture.storage = {GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);
        assert(depth_texture.texture != 0);

        auto fbo = gfx::FBO{};
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, frontface_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, backface_texture);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["bounding_geometry"] = fbo;
    }

    { // deep shadow map (DSM) bounding geometry
        auto frontface_texture = gfx::Texture2D{};
        frontface_texture.target = GL_TEXTURE_2D;
        frontface_texture.width = ctx.settings.dsm.width;
        frontface_texture.height = ctx.settings.dsm.height;
        frontface_texture.storage = {GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT, 1};
        frontface_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(frontface_texture);
        assert(frontface_texture.texture != 0);

        auto backface_texture = gfx::Texture2D{};
        backface_texture.target = GL_TEXTURE_2D;
        backface_texture.width = ctx.settings.dsm.width;
        backface_texture.height = ctx.settings.dsm.height;
        backface_texture.storage = {GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT, 1};
        backface_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(backface_texture);
        assert(backface_texture.texture != 0);

        auto depth_texture = gfx::Texture2D{};
        depth_texture.target = GL_TEXTURE_2D;
        depth_texture.width = ctx.settings.dsm.width;
        depth_texture.height = ctx.settings.dsm.height;
        depth_texture.storage = {GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT, 1};
        depth_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(depth_texture);
        assert(depth_texture.texture != 0);

        auto fbo = gfx::FBO{};
        gfx::fbo_create(fbo, ctx.settings.dsm.width, ctx.settings.dsm.height, false);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, frontface_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, backface_texture);
        gfx::fbo_attach_texture(fbo, GL_DEPTH_ATTACHMENT, depth_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["dsm_bounding_geometry"] = fbo;
    }

    { // scene
        auto color_texture = gfx::Texture2D{};
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);
        assert(color_texture.texture != 0);

        auto fbo = gfx::FBO{};
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["scene"] = fbo;
    }

    { // bloom
        const uint32_t width = ctx.window.width / ctx.settings.bloom.downsampling;
        const uint32_t height = ctx.window.height / ctx.settings.bloom.downsampling;

        auto color_texture = gfx::Texture2D{};
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = width;
        color_texture.height = height;
        color_texture.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);
        assert(color_texture.texture != 0);

        auto color_texture2 = gfx::Texture2D{};
        color_texture2.target = GL_TEXTURE_2D;
        color_texture2.width = width;
        color_texture2.height = height;
        color_texture2.storage = {GL_R11F_G11F_B10F, GL_RGB, GL_FLOAT, 1};
        color_texture2.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture2);
        assert(color_texture2.texture != 0);

        auto fbo = gfx::FBO{};
        gfx::fbo_create(fbo, width, height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT1, color_texture2);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["bloom"] = fbo;
    }

    { // tone mapping
        auto color_texture = gfx::Texture2D{};
        color_texture.target = GL_TEXTURE_2D;
        color_texture.width = ctx.window.width;
        color_texture.height = ctx.window.height;
        color_texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
        color_texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
        gfx::texture_2d_create(color_texture);
        assert(color_texture.texture != 0);

        auto fbo = gfx::FBO{};
        gfx::fbo_create(fbo, ctx.window.width, ctx.window.height, true);
        gfx::fbo_attach_texture(fbo, GL_COLOR_ATTACHMENT0, color_texture);
        assert(gfx::fbo_is_complete(fbo));
        ctx.fbos["tone_mapping"] = fbo;
    }
}

void init_deep_shadow_map_texture(Context &ctx)
{
    auto dsm_texture = gfx::Texture2DArray{};
    dsm_texture.target = GL_TEXTURE_2D_ARRAY;
    dsm_texture.width = ctx.settings.dsm.width;
    dsm_texture.height = ctx.settings.dsm.height;
    dsm_texture.depth = ctx.settings.dsm.num_layers;
    dsm_texture.storage = {GL_RG16, GL_RG, GL_UNSIGNED_SHORT, 1};
    dsm_texture.sampling = {GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER};
    gfx::texture_2d_array_create(dsm_texture);
    assert(dsm_texture.texture != 0);

    ctx.dsm_texture = dsm_texture;
}

void recreate_deep_shadow_map_texture(Context &ctx)
{
    glDeleteTextures(1, &ctx.dsm_texture.texture);
    init_deep_shadow_map_texture(ctx);
}

void init_dsm_alpha_error(Context &ctx)
{
    ctx.settings.dsm.alpha_error = 1.0f / ctx.settings.dsm.num_layers;
}

void load_volume(Context &ctx)
{
    pbrvol::load_uint8_vtk_volume(ctx.volume_filename.c_str(), ctx.volume);

    glDeleteTextures(1, &ctx.volume_texture.texture);
    ctx.volume_texture =
        pbrvol::create_uint8_volume_texture(ctx.volume.data, ctx.volume.dimensions);

    pbrvol::create_max_blocks(ctx.volume, glm::uvec3(32, 32, 32), ctx.max_block_volume);

    ctx.bounding_geometry_is_dirty = true;
    ctx.isotropic_halfres_volume_texture_is_dirty = true;
    ctx.ao_is_dirty = true;
}

void init_camera(Context &ctx)
{
    ctx.camera.eye = glm::vec3(0.0f, 0.0f, 600.0f);
    ctx.camera.center = glm::vec3(0.0f, 0.0f, 0.0f);
    ctx.camera.up = glm::vec3(0.0f, 1.0f, 0.0f);
    ctx.camera.fovy = glm::radians(45.0f);
    ctx.camera.aspect = (float)ctx.window.width / ctx.window.height;
    ctx.camera.z_near = 1.0f;
    ctx.camera.z_far = 1000.0f;

    const float bsphere_radius = glm::length(0.5f * pbrvol::get_volume_extent(ctx.volume));
    const glm::vec3 bsphere_center = {0.0f, 0.0f, 0.0f};
    pbrvol::fit_frustum_to_bsphere(bsphere_radius, bsphere_center, ctx.camera);
}

void init_key_light(Context &ctx)
{
    ctx.key_light.direction = glm::vec3(-2.0f, -2.0f, -4.0f);
    ctx.key_light.color = glm::vec3(1.0f, 1.0f, 1.0f);
    ctx.key_light.intensity = 25000.0f;
}

void init_key_light_camera(Context &ctx)
{
    const float bsphere_radius = glm::length(0.5f * pbrvol::get_volume_extent(ctx.volume));
    const glm::vec3 bsphere_center = {0.0f, 0.0f, 0.0f};

    const glm::vec3 center = bsphere_center;
    const glm::vec3 eye = center - bsphere_radius * glm::normalize(ctx.key_light.direction);
    const glm::vec3 up = {0.0f, 1.0f, 0.0f};
    ctx.key_light_camera.view_matrix = glm::lookAt(eye, center, up);

    const float frustum_width = 2.0f * bsphere_radius;
    const float frustum_height = 2.0f * bsphere_radius;
    const float left = -0.5f * frustum_width;
    const float right = 0.5f * frustum_width;
    const float bottom = -0.5f * frustum_height;
    const float top = 0.5f * frustum_height;
    const float z_near = 1.0f;
    const float z_far = 2.0f * bsphere_radius;
    ctx.key_light_camera.projection_matrix = glm::ortho(left, right, bottom, top, z_near, z_far);
}

void init_timer_queries(Context &ctx)
{
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_tic);
    glGenQueries(NUM_TIME_STAMPS, ctx.timer_queries_toc);
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        ctx.frame_times_ms[i] = 0.0f;
    }
}

void init(Context &ctx)
{
    glCreateVertexArrays(1, &ctx.default_vao);
    ctx.quad_vao = pbrvol::create_quad_vao();

    load_shader_programs(ctx);

    init_fbos(ctx);

    init_deep_shadow_map_texture(ctx);
    init_dsm_alpha_error(ctx);

    load_volume(ctx);

    ctx.jitter_texture = pbrvol::load_rgba8_texture(
        root_dir() + "/resources/noise_textures/blue_noise_64x64_rgba.png");

    ctx.tf_points = pbrvol::get_default_transfer_function();
    pbrvol::create_tf_texture(ctx.tf_points, ctx.tf_texture);

    const std::vector<pbrvol::OpacityTFPoint> opacity_tf_points = {{ctx.settings.tf_alpha1, 0.0f},
                                                                   {ctx.settings.tf_alpha2, 1.0f}};
    pbrvol::create_opacity_tf_texture(opacity_tf_points, ctx.opacity_tf_texture);

    ctx.longlat_envmap_texture = pbrvol::load_longlat_hdr_texture(
        root_dir() + "/resources/envmaps/abandoned_slipway_2k.hdr");

    init_camera(ctx);
    init_key_light(ctx);
    init_key_light_camera(ctx);

    init_timer_queries(ctx);
}

void update_shared_transforms(Context &ctx)
{
    ctx.shared_transforms.view_from_world =
        glm::lookAt(ctx.camera.eye, ctx.camera.center, ctx.camera.up);
    ctx.shared_transforms.proj_from_view =
        glm::perspective(ctx.camera.fovy, ctx.camera.aspect, ctx.camera.z_near, ctx.camera.z_far);

    ctx.shared_transforms.key_light_view_from_world = ctx.key_light_camera.view_matrix;
    ctx.shared_transforms.key_light_proj_from_view = ctx.key_light_camera.projection_matrix;
}

void update_volume_transform(Context &ctx)
{
    ctx.volume_world_from_model =
        gfx::trackball_get_rotation_matrix(ctx.trackball) *
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)) *
        pbrvol::get_volume_matrix(ctx.volume);
}

void update_settings_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.settings_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.settings_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.settings_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Settings", &ctx.ui.show_settings_ui);

    if (ImGui::CollapsingHeader("Volume rendering")) {
        ImGui::Combo("Display mode", (int32_t *)(&ctx.settings.display_mode),
                     "Ray-casting\0Front faces\0Back faces\0");
        ImGui::SliderFloat("Step size (voxels)", &ctx.settings.step_length_voxels, 0.25f, 2.0f);
        ImGui::Checkbox("Jittering", &ctx.settings.jittering_enabled);
    }

    if (ImGui::CollapsingHeader("Material")) {
        ImGui::SliderFloat("Shininess", &ctx.settings.gloss, 0.0f, 1.0f);
        ImGui::SliderFloat("Scatter factor", &ctx.settings.scatter_factor, 0.0f, 2.0f);
        ImGui::SliderFloat("Wrap", &ctx.settings.wrap, 0.0f, 2.0f);
    }

    if (ImGui::CollapsingHeader("Shadows")) {
        ImGui::Checkbox("AO", &ctx.settings.ao.enabled);
        if (ImGui::SliderFloat("AO radius (mm)", &ctx.settings.ao.radius_mm, 1.0f, 100.0f)) {
            ctx.ao_is_dirty = true;
        }
        if (ImGui::SliderFloat("AO normal factor", &ctx.settings.ao.normal_factor, 0.0f, 40.0f)) {
            ctx.ao_is_dirty = true;
        }
        ImGui::Checkbox("DSM", &ctx.settings.dsm.enabled);
        ImGui::SliderFloat("DSM step size (voxels)", &ctx.settings.dsm.step_size_voxels, 0.25f,
                           2.0f);
        if (ImGui::SliderInt("DSM num layers", (int32_t *)&ctx.settings.dsm.num_layers, 1, 16)) {
            recreate_deep_shadow_map_texture(ctx);
            init_dsm_alpha_error(ctx);
        }
        ImGui::SliderFloat("DSM bias", &ctx.settings.dsm.bias, 0.0f, 1.0f);
        ImGui::SliderFloat("DSM alpha error", &ctx.settings.dsm.alpha_error, 0.0f, 1.0f);
    }

    if (ImGui::CollapsingHeader("Lighting")) {
        ImGui::ColorEdit3("Key light color", &ctx.key_light.color[0]);
        ImGui::SliderFloat("Key light intensity (lux)", &ctx.key_light.intensity, 0.0f, 200000.0f);

        ImGui::SliderFloat("Ambient light intensity (lux)", &ctx.settings.ambient_light_intensity,
                           0.0f, 200000.0f);

        ImGui::Combo("Background mode", (int32_t *)(&ctx.settings.background.mode),
                     "Environment map\0Solid color\0");
        ImGui::ColorEdit3("Background color", &ctx.settings.background.color[0]);
        ImGui::SliderFloat("Background color intensity", &ctx.settings.background.intensity, 0.0f,
                           200000.0f);
    }

    if (ImGui::CollapsingHeader("Camera")) {
        ImGui::SliderFloat("Aperture", &ctx.settings.camera.aperture, 1.0f, 22.0f);
        ImGui::SliderFloat("Shutter time (s)", &ctx.settings.camera.shutter_time, 0.001f, 1.0f);
        ImGui::SliderFloat("ISO", &ctx.settings.camera.iso, 50.0f, 1600.0f);
        ImGui::SliderFloat("EC (-/+)", &ctx.settings.camera.exposure_compensation, -4.0f, 4.0f);
    }

    if (ImGui::CollapsingHeader("PostFX")) {
        ImGui::Checkbox("Bloom", &ctx.settings.bloom.enabled);
        ImGui::SliderFloat("Bloom intensity", &ctx.settings.bloom.intensity, 0.0f, 1.0f);
        ImGui::Combo("Tone mapping operator", (int32_t *)(&ctx.settings.tone_mapping_op),
                     "Aces\0Unreal\0Gamma-only\0None\0");
        ImGui::Checkbox("FXAA", &ctx.settings.fxaa_enabled);
    }

    if (ImGui::CollapsingHeader("Info")) {
        ImGui::Text("Viewport resolution: %dx%d", ctx.window.width, ctx.window.height);
        ImGui::Text("Volume dimensions: %dx%dx%d", ctx.volume.dimensions[0],
                    ctx.volume.dimensions[1], ctx.volume.dimensions[2]);
        ImGui::Text("Voxel size: %.3f %.3f %.3f", ctx.volume.spacing[0], ctx.volume.spacing[1],
                    ctx.volume.spacing[2]);
    }

    ImGui::End();
}

void imgui_profiler_bar(const char *label, const float time_in_ms, const ImVec4 &color)
{
    const float cursor_pos_x = ImGui::GetCursorPosX();
    ImGui::ColorButton("", color, 0, ImVec2(std::fmin(10.0f * time_in_ms, 1000.0), 0.0f));
    ImGui::SameLine();
    ImGui::SetCursorPosX(cursor_pos_x + 2.0f);
    ImGui::Text("%s %.1f", label, time_in_ms);
}

void update_profiler_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.profiler_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.profiler_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.profiler_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Frame times (ms)", &ctx.ui.show_profiler_ui);

    const ImVec4 color = {0.7f, 0.5f, 0.1f, 1.0f};
    imgui_profiler_bar("Bounding geometry", ctx.frame_times_ms[TIME_STAMP_BOUNDING_GEOMETRY],
                       color);
    imgui_profiler_bar("Bounding geometry (DSM)",
                       ctx.frame_times_ms[TIME_STAMP_BOUNDING_GEOMETRY_DSM], color);
    imgui_profiler_bar("DSM", ctx.frame_times_ms[TIME_STAMP_DSM], color);
    imgui_profiler_bar("Background", ctx.frame_times_ms[TIME_STAMP_BACKGROUND], color);
    imgui_profiler_bar("Ray casting", ctx.frame_times_ms[TIME_STAMP_RAY_CASTING], color);
    imgui_profiler_bar("Bloom", ctx.frame_times_ms[TIME_STAMP_BLOOM], color);
    imgui_profiler_bar("Tone mapping", ctx.frame_times_ms[TIME_STAMP_TONE_MAPPING], color);
    imgui_profiler_bar("FXAA", ctx.frame_times_ms[TIME_STAMP_FXAA], color);
    imgui_profiler_bar("Total", ctx.frame_times_ms[TIME_STAMP_TOTAL], color);

    ImGui::End();
}

void update_tf_ui(Context &ctx)
{
    ImGui::SetNextWindowSize(ctx.ui.tf_ui_size, ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ctx.ui.tf_ui_pos, ImGuiSetCond_Once);
    ImGui::SetNextWindowCollapsed(ctx.ui.tf_ui_collapsed, ImGuiSetCond_Once);
    ImGui::SetNextWindowBgAlpha(0.7f);
    ImGui::Begin("Transfer function", &ctx.ui.show_tf_ui);

    if (ImGui::CollapsingHeader("Color TF")) {
        ImGui::Checkbox("Display TF texture", (bool *)&ctx.settings.display_transfer_function);
        ImGui::Separator();

        for (uint32_t i = 0; i < ctx.tf_points.size(); ++i) {
            ImGui::PushID(i);
            ImGui::Text("TF point %d:", i);
            if (ImGui::ColorEdit3("Albedo", &ctx.tf_points[i].material.albedo[0])) {
                ctx.tf_texture_is_dirty = true;
            }
            if (ImGui::SliderFloat("Position", &ctx.tf_points[i].position, 0.0f, 1.0f)) {
                ctx.tf_texture_is_dirty = true;
            }
            ImGui::PopID();
        }
    }

    if (ImGui::CollapsingHeader("Opacity TF")) {
        if (ImGui::SliderFloat("TF alpha 1", &ctx.settings.tf_alpha1, 0.0f, 1.0f)) {
            ctx.opacity_tf_texture_is_dirty = true;
            ctx.ao_is_dirty = true;
            ctx.bounding_geometry_is_dirty = true;
        }
        if (ImGui::SliderFloat("TF alpha 2", &ctx.settings.tf_alpha2, 0.0f, 1.0f)) {
            ctx.opacity_tf_texture_is_dirty = true;
            ctx.ao_is_dirty = true;
        }
    }

    ImGui::End();
}

void update_ui(Context &ctx)
{
    update_settings_ui(ctx);
    update_profiler_ui(ctx);
    update_tf_ui(ctx);
}

void update_color_tf_texture(Context &ctx)
{
    if (!ctx.tf_texture_is_dirty)
        return;

    glDeleteTextures(1, &ctx.tf_texture.texture);
    pbrvol::create_tf_texture(ctx.tf_points, ctx.tf_texture);
    ctx.tf_texture_is_dirty = false;
}

void update_opacity_tf_texture(Context &ctx)
{
    if (!ctx.opacity_tf_texture_is_dirty)
        return;

    glDeleteTextures(1, &ctx.opacity_tf_texture.texture);
    const std::vector<pbrvol::OpacityTFPoint> opacity_tf_points = {{ctx.settings.tf_alpha1, 0.0f},
                                                                   {ctx.settings.tf_alpha2, 1.0f}};
    pbrvol::create_opacity_tf_texture(opacity_tf_points, ctx.opacity_tf_texture);
    ctx.opacity_tf_texture_is_dirty = false;
}

void update_bounding_geometry(Context &ctx)
{
    if (!ctx.bounding_geometry_is_dirty)
        return;

    pbrvol::delete_bounding_geometry(ctx.bounding_geometry);
    assert(ctx.settings.tf_alpha1 >= 0.0f);
    const auto threshold = static_cast<uint32_t>(ctx.settings.tf_alpha1 * 255.99f);
    ctx.bounding_geometry = pbrvol::create_bounding_geometry(ctx.max_block_volume, threshold);
    ctx.bounding_geometry_is_dirty = false;
}

void update_isotropic_halfres_volume_texture(Context &ctx)
{
    if (!ctx.isotropic_halfres_volume_texture_is_dirty)
        return;

    GLuint timer_queries[2];
    glGenQueries(2, timer_queries);

    LOG_INFO("Downsampling volume...\n");
    glQueryCounter(timer_queries[0], GL_TIMESTAMP);
    glDeleteTextures(1, &ctx.isotropic_halfres_volume_texture.texture);
    ctx.isotropic_halfres_volume_texture =
        pbrvol::downsample_volume(ctx.programs, ctx.volume_texture, ctx.volume.spacing);
    glQueryCounter(timer_queries[1], GL_TIMESTAMP);

    GLuint64 tic_ns;
    glGetQueryObjectui64v(timer_queries[0], GL_QUERY_RESULT, &tic_ns);
    GLuint64 toc_ns;
    glGetQueryObjectui64v(timer_queries[1], GL_QUERY_RESULT, &toc_ns);
    glDeleteQueries(2, timer_queries);
    LOG_INFO("Elapsed time (ms): %f\n", double(toc_ns - tic_ns) / 1e6);

    ctx.isotropic_halfres_volume_texture_is_dirty = false;
}

void update_ao_texture(Context &ctx)
{
    if (!ctx.ao_is_dirty)
        return;

    GLuint timer_queries[2];
    glGenQueries(2, timer_queries);

    LOG_INFO("Updating AO texture...\n");
    glQueryCounter(timer_queries[0], GL_TIMESTAMP);
    glDeleteTextures(1, &ctx.ao_texture.texture);
    const glm::vec3 spacing = ctx.volume.spacing * glm::vec3(ctx.volume.dimensions) /
                              glm::vec3(ctx.isotropic_halfres_volume_texture.width,
                                        ctx.isotropic_halfres_volume_texture.height,
                                        ctx.isotropic_halfres_volume_texture.depth);
    ctx.ao_texture = pbrvol::compute_ambient_occlusion(
        ctx.programs, ctx.isotropic_halfres_volume_texture, ctx.opacity_tf_texture, spacing,
        ctx.settings.ao.normal_factor, ctx.settings.ao.radius_mm);
    glQueryCounter(timer_queries[1], GL_TIMESTAMP);

    GLuint64 tic_ns;
    glGetQueryObjectui64v(timer_queries[0], GL_QUERY_RESULT, &tic_ns);
    GLuint64 toc_ns;
    glGetQueryObjectui64v(timer_queries[1], GL_QUERY_RESULT, &toc_ns);
    glDeleteQueries(2, timer_queries);
    LOG_INFO("Elapsed time (ms): %f\n", double(toc_ns - tic_ns) / 1e6);

    ctx.ao_is_dirty = false;
}

void update_exposure(Context &ctx)
{
    ctx.settings.exposure = pbrvol::compute_exposure(
        ctx.settings.camera.aperture, ctx.settings.camera.shutter_time, ctx.settings.camera.iso);
}

void update_cubemap_texture(Context &ctx)
{
    if (!ctx.cubemap_texture_is_dirty)
        return;

    glDeleteTextures(1, &ctx.cubemap_texture.texture);
    ctx.cubemap_texture =
        pbrvol::resample_to_cubemap(ctx.programs["longlat_to_cubemap"], ctx.longlat_envmap_texture);
    ctx.cubemap_texture_is_dirty = false;
}

void update(Context &ctx)
{
    update_ui(ctx);
    update_color_tf_texture(ctx);
    update_opacity_tf_texture(ctx);
    update_bounding_geometry(ctx);
    update_isotropic_halfres_volume_texture(ctx);
    update_ao_texture(ctx);
    update_shared_transforms(ctx);
    update_volume_transform(ctx);
    update_exposure(ctx);
    update_cubemap_texture(ctx);
}

void draw_bounding_geometry(const GLuint program,
                            const pbrvol::BoundingGeometryVao &bounding_geometry,
                            const glm::mat4 &world_from_model, const glm::mat4 &view_from_world,
                            const glm::mat4 &proj_from_view)
{
    const glm::mat4 view_from_model = view_from_world * world_from_model;

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &view_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &proj_from_view[0][0]);
    glUseProgram(program);
    glBindVertexArray(bounding_geometry.vao);
    glDrawElements(GL_TRIANGLES, bounding_geometry.num_indices, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void dispatch_deep_shadow_map(
    const GLuint program, const gfx::Texture2D &frontface_texture,
    const gfx::Texture2D &backface_texture, const gfx::Texture3D &volume_texture,
    const gfx::Texture2D &jitter_texture, const gfx::Texture2D &opacity_tf_texture,
    const gfx::Texture2DArray &dsm_texture, const glm::mat4 &world_from_model,
    const glm::mat4 &light_view_from_world, const glm::mat4 &light_proj_from_view,
    const glm::vec3 &spacing, const Context &ctx)
{
    const glm::mat4 model_from_world = glm::inverse(world_from_model);
    const glm::mat4 light_proj_from_model =
        light_proj_from_view * light_view_from_world * world_from_model;

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &world_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &model_from_world[0][0]);
    glProgramUniformMatrix4fv(program, 2, 1, GL_FALSE, &light_proj_from_model[0][0]);
    glProgramUniform1f(program, 3, ctx.settings.dsm.step_size_voxels);
    glProgramUniform3fv(program, 4, 1, &spacing[0]);
    glProgramUniform1i(program, 5, ctx.settings.jittering_enabled);
    glProgramUniform1f(program, 6, ctx.settings.dsm.alpha_error);

    glBindTextureUnit(0, frontface_texture.texture);
    glBindTextureUnit(1, backface_texture.texture);
    glBindTextureUnit(2, volume_texture.texture);
    glBindTextureUnit(3, jitter_texture.texture);
    glBindTextureUnit(4, opacity_tf_texture.texture);
    glBindImageTexture(0, dsm_texture.texture, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RG16);

    glUseProgram(program);
    glDispatchCompute(dsm_texture.width / 8 + 1, dsm_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_background(const GLuint program, const gfx::TextureCubemap &envmap_texture,
                         const gfx::Texture2D &output_texture, const Context &ctx)
{
    // We render the low resolution cubemap at mip level 1.5 to blur it a bit (quick and dirty DoF)
    // and hide the blockiness of linear interpolation. Would be better to prefilter the cubemap
    // with a Gaussian filter, but this is good enough for now...
    const float mip_level = 1.5f;

    glProgramUniform1i(program, 0, int(ctx.settings.background.mode));
    glProgramUniform1f(program, 1, ctx.settings.ambient_light_intensity);
    glProgramUniform1f(program, 2, mip_level);
    glProgramUniform3fv(program, 3, 1, &ctx.settings.background.color[0]);
    glProgramUniform1f(program, 4, ctx.settings.background.intensity);
    glProgramUniform1f(program, 5, ctx.settings.exposure);
    glProgramUniform3fv(program, 6, 1, &ctx.camera.eye[0]);
    glProgramUniform3fv(program, 7, 1, &ctx.camera.center[0]);
    glProgramUniform3fv(program, 8, 1, &ctx.camera.up[0]);
    glProgramUniform1f(program, 9, ctx.camera.fovy);
    glProgramUniform1f(program, 10, ctx.camera.aspect);

    glBindTextureUnit(0, envmap_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);

    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_ray_casting(const GLuint program, const gfx::Texture2D &frontface_texture,
                          const gfx::Texture2D &backface_texture,
                          const gfx::Texture3D &volume_texture,
                          const gfx::Texture2D &jitter_texture, const gfx::Texture2D &tf_texture,
                          const gfx::Texture2D &opacity_tf_texture,
                          const gfx::TextureCubemap &cubemap_texture,
                          const gfx::Texture3D &ao_texture, const gfx::Texture2DArray &dsm_texture,
                          const gfx::Texture2D &output_texture, const glm::vec3 &voxel_spacing,
                          const glm::mat4 &world_from_model, const Context &ctx)
{
    const glm::mat4 model_from_world = glm::inverse(world_from_model);
    const glm::mat3 world_normal_from_model = glm::inverseTranspose(glm::mat3(world_from_model));
    const glm::mat4 light_proj_from_model = ctx.shared_transforms.key_light_proj_from_view *
                                            ctx.shared_transforms.key_light_view_from_world *
                                            world_from_model;
    const glm::vec3 key_light_color = pbrvol::srgb2lin(ctx.key_light.color);

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &world_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 1, 1, GL_FALSE, &model_from_world[0][0]);
    glProgramUniformMatrix3fv(program, 2, 1, GL_FALSE, &world_normal_from_model[0][0]);
    glProgramUniformMatrix4fv(program, 3, 1, GL_FALSE, &light_proj_from_model[0][0]);
    glProgramUniform1f(program, 4, ctx.settings.step_length_voxels);
    glProgramUniform3fv(program, 5, 1, &voxel_spacing[0]);
    glProgramUniform1i(program, 6, ctx.settings.jittering_enabled);
    glProgramUniform1f(program, 7, ctx.settings.gloss);
    glProgramUniform1f(program, 8, ctx.settings.wrap);
    glProgramUniform1f(program, 9, ctx.settings.scatter_factor);
    glProgramUniform1i(program, 10, ctx.settings.ao.enabled);
    glProgramUniform1i(program, 11, ctx.settings.dsm.enabled);
    glProgramUniform1f(program, 12, ctx.settings.dsm.bias);
    glProgramUniform3fv(program, 13, 1, &ctx.key_light.direction[0]);
    glProgramUniform3fv(program, 14, 1, &key_light_color[0]);
    glProgramUniform1f(program, 15, ctx.key_light.intensity);
    glProgramUniform1f(program, 16, ctx.settings.ambient_light_intensity);
    glProgramUniform1f(program, 17, ctx.settings.exposure);

    glBindTextureUnit(0, frontface_texture.texture);
    glBindTextureUnit(1, backface_texture.texture);
    glBindTextureUnit(2, volume_texture.texture);
    glBindTextureUnit(3, jitter_texture.texture);
    glBindTextureUnit(4, tf_texture.texture);
    glBindTextureUnit(5, opacity_tf_texture.texture);
    glBindTextureUnit(6, cubemap_texture.texture);
    glBindTextureUnit(7, ao_texture.texture);
    glBindTextureUnit(8, dsm_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);

    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_bloom_brightpass(const GLuint program, const gfx::Texture2D &input_texture,
                               const gfx::Texture2D &output_texture, const Context &ctx)
{
    glProgramUniform1f(program, 0, ctx.settings.bloom.intensity);
    glBindTextureUnit(0, input_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R11F_G11F_B10F);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_gaussian_blur(const GLuint program, const gfx::Texture2D &input_texture,
                            const gfx::Texture2D &output_texture, const glm::vec2 &direction)
{
    glProgramUniform2fv(program, 0, 1, &direction[0]);
    glBindTextureUnit(0, input_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R11F_G11F_B10F);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_bloom_blend(const GLuint program, const gfx::Texture2D &bloom_texture,
                          const gfx::Texture2D &scene_texture)
{
    glBindTextureUnit(0, bloom_texture.texture);
    glBindImageTexture(0, scene_texture.texture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glUseProgram(program);
    glDispatchCompute(scene_texture.width / 8 + 1, scene_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void dispatch_tone_mapping(const GLuint program, const gfx::Texture2D &input_texture,
                           const gfx::Texture2D &output_texture, const Context &ctx)
{
    glProgramUniform1i(program, 0, ctx.settings.tone_mapping_op);
    glProgramUniform1f(program, 1, ctx.settings.camera.exposure_compensation);
    glBindTextureUnit(0, input_texture.texture);
    glBindImageTexture(0, output_texture.texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
    glUseProgram(program);
    glDispatchCompute(output_texture.width / 8 + 1, output_texture.height / 8 + 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

void draw_fxaa(const GLuint program, const gfx::Texture2D &texture)
{
    glBindTextureUnit(0, texture.texture);
    glUseProgram(program);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void draw_transfer_function(const GLuint program, const GLuint quad_vao,
                            const gfx::Texture2D &tf_texture)
{
    const glm::mat4 ndc_from_model =
        glm::scale(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -0.85f, 0.0f)),
                   glm::vec3(0.04f, 0.1f, 1.0f));

    glProgramUniformMatrix4fv(program, 0, 1, GL_FALSE, &ndc_from_model[0][0]);
    glBindTextureUnit(0, tf_texture.texture);
    glUseProgram(program);
    glBindVertexArray(quad_vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

void reset_gl_states()
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glDisable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
}

void reset_timer_queries(Context &ctx)
{
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        glQueryCounter(ctx.timer_queries_tic[i], GL_TIMESTAMP);
        glQueryCounter(ctx.timer_queries_toc[i], GL_TIMESTAMP);
    }
}

void update_frame_times(Context &ctx)
{
    for (uint32_t i = 0; i < NUM_TIME_STAMPS; ++i) {
        GLuint64 tic_ns;
        glGetQueryObjectui64v(ctx.timer_queries_tic[i], GL_QUERY_RESULT, &tic_ns);
        GLuint64 toc_ns;
        glGetQueryObjectui64v(ctx.timer_queries_toc[i], GL_QUERY_RESULT, &toc_ns);
        const float elapsed_time_ms = toc_ns > tic_ns ? float(toc_ns - tic_ns) / 1.0e6f : 0.0f;
        ctx.frame_times_ms[i] = 0.2f * elapsed_time_ms + 0.8f * ctx.frame_times_ms[i];
    }
}

void display(Context &ctx)
{
    glBindVertexArray(ctx.default_vao);
    reset_gl_states();

    reset_timer_queries(ctx);
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TOTAL], GL_TIMESTAMP);

    { // Clear default screen framebuffer
        auto clear_color = glm::vec4{0.0f, 0.0f, 0.0f, 0.0f};
        glClearNamedFramebufferfv(0, GL_COLOR, 0, &clear_color[0]);
        glClearNamedFramebufferfi(0, GL_DEPTH_STENCIL, 0, 1.0f, 0);
    }

    // Draw bounding geometry
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BOUNDING_GEOMETRY], GL_TIMESTAMP);
    {
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["bounding_geometry"].fbo);
        auto clear_value = glm::vec4{0.0f, 0.0f, 0.0f, 0.0f};

        // Frontfaces
        glNamedFramebufferDrawBuffer(ctx.fbos["bounding_geometry"].fbo, GL_COLOR_ATTACHMENT0);
        glClearNamedFramebufferfv(ctx.fbos["bounding_geometry"].fbo, GL_COLOR, 0, &clear_value[0]);
        glClearNamedFramebufferfi(ctx.fbos["bounding_geometry"].fbo, GL_DEPTH_STENCIL, 0, 1.0f, 0);
        draw_bounding_geometry(ctx.programs["bounding_geometry"], ctx.bounding_geometry,
                               ctx.volume_world_from_model, ctx.shared_transforms.view_from_world,
                               ctx.shared_transforms.proj_from_view);

        // Backfaces
        glCullFace(GL_FRONT);
        glDepthFunc(GL_GREATER);
        glNamedFramebufferDrawBuffer(ctx.fbos["bounding_geometry"].fbo, GL_COLOR_ATTACHMENT1);
        glClearNamedFramebufferfv(ctx.fbos["bounding_geometry"].fbo, GL_COLOR, 0, &clear_value[0]);
        glClearNamedFramebufferfi(ctx.fbos["bounding_geometry"].fbo, GL_DEPTH_STENCIL, 0, 0.0f, 0);
        draw_bounding_geometry(ctx.programs["bounding_geometry"], ctx.bounding_geometry,
                               ctx.volume_world_from_model, ctx.shared_transforms.view_from_world,
                               ctx.shared_transforms.proj_from_view);

        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BOUNDING_GEOMETRY], GL_TIMESTAMP);

    // Draw bounding geometry as seen from the key light (for deep shadow map)
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BOUNDING_GEOMETRY_DSM], GL_TIMESTAMP);
    {
        glViewport(0, 0, ctx.settings.dsm.width, ctx.settings.dsm.height);
        glBindFramebuffer(GL_FRAMEBUFFER, ctx.fbos["dsm_bounding_geometry"].fbo);
        auto clear_value = glm::vec4{0.0f, 0.0f, 0.0f, 0.0f};

        // Frontfaces
        glNamedFramebufferDrawBuffer(ctx.fbos["dsm_bounding_geometry"].fbo, GL_COLOR_ATTACHMENT0);
        glClearNamedFramebufferfv(ctx.fbos["dsm_bounding_geometry"].fbo, GL_COLOR, 0,
                                  &clear_value[0]);
        glClearNamedFramebufferfi(ctx.fbos["dsm_bounding_geometry"].fbo, GL_DEPTH_STENCIL, 0, 1.0f,
                                  0);
        draw_bounding_geometry(ctx.programs["bounding_geometry"], ctx.bounding_geometry,
                               ctx.volume_world_from_model,
                               ctx.shared_transforms.key_light_view_from_world,
                               ctx.shared_transforms.key_light_proj_from_view);

        // Backfaces
        glCullFace(GL_FRONT);
        glDepthFunc(GL_GREATER);
        glNamedFramebufferDrawBuffer(ctx.fbos["dsm_bounding_geometry"].fbo, GL_COLOR_ATTACHMENT1);
        glClearNamedFramebufferfv(ctx.fbos["dsm_bounding_geometry"].fbo, GL_COLOR, 0,
                                  &clear_value[0]);
        glClearNamedFramebufferfi(ctx.fbos["dsm_bounding_geometry"].fbo, GL_DEPTH_STENCIL, 0, 0.0f,
                                  0);
        draw_bounding_geometry(ctx.programs["bounding_geometry"], ctx.bounding_geometry,
                               ctx.volume_world_from_model,
                               ctx.shared_transforms.key_light_view_from_world,
                               ctx.shared_transforms.key_light_proj_from_view);

        glViewport(0, 0, ctx.window.width, ctx.window.height);
        reset_gl_states();
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BOUNDING_GEOMETRY_DSM], GL_TIMESTAMP);

    // Generate deep shadow map
    glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_DSM], GL_TIMESTAMP);
    if (ctx.settings.dsm.enabled > 0) {
        dispatch_deep_shadow_map(
            ctx.programs["deep_shadow_map"],
            ctx.fbos["dsm_bounding_geometry"].attachments[GL_COLOR_ATTACHMENT0],
            ctx.fbos["dsm_bounding_geometry"].attachments[GL_COLOR_ATTACHMENT1], ctx.volume_texture,
            ctx.jitter_texture, ctx.opacity_tf_texture, ctx.dsm_texture,
            ctx.volume_world_from_model, ctx.shared_transforms.key_light_view_from_world,
            ctx.shared_transforms.key_light_proj_from_view, ctx.volume.spacing, ctx);
    }
    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_DSM], GL_TIMESTAMP);

    if (ctx.settings.display_mode == DISPLAY_MODE_RAY_CASTING) {
        // Render background/skybox
        glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BACKGROUND], GL_TIMESTAMP);
        dispatch_background(ctx.programs["background"], ctx.cubemap_texture,
                            ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0], ctx);
        glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BACKGROUND], GL_TIMESTAMP);

        // Render volume and composite with background
        glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_RAY_CASTING], GL_TIMESTAMP);
        dispatch_ray_casting(ctx.programs["ray_caster"],
                             ctx.fbos["bounding_geometry"].attachments[GL_COLOR_ATTACHMENT0],
                             ctx.fbos["bounding_geometry"].attachments[GL_COLOR_ATTACHMENT1],
                             ctx.volume_texture, ctx.jitter_texture, ctx.tf_texture,
                             ctx.opacity_tf_texture, ctx.cubemap_texture, ctx.ao_texture,
                             ctx.dsm_texture, ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                             ctx.volume.spacing, ctx.volume_world_from_model, ctx);
        glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_RAY_CASTING], GL_TIMESTAMP);

        // Apply bloom
        glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_BLOOM], GL_TIMESTAMP);
        if (ctx.settings.bloom.enabled) {
            dispatch_bloom_brightpass(ctx.programs["bloom_brightpass"],
                                      ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                                      ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0], ctx);

            for (uint32_t i = 0; i < ctx.settings.bloom.num_iterations; ++i) {
                dispatch_gaussian_blur(ctx.programs["gaussian_blur"],
                                       ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                                       ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT1],
                                       glm::vec2(1.0f, 0.0f));

                dispatch_gaussian_blur(ctx.programs["gaussian_blur"],
                                       ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT1],
                                       ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                                       glm::vec2(0.0f, 1.0f));
            }

            dispatch_bloom_blend(ctx.programs["bloom_blend"],
                                 ctx.fbos["bloom"].attachments[GL_COLOR_ATTACHMENT0],
                                 ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0]);
        }
        glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_BLOOM], GL_TIMESTAMP);

        // Apply tone mapping
        glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);
        dispatch_tone_mapping(ctx.programs["tone_mapping"],
                              ctx.fbos["scene"].attachments[GL_COLOR_ATTACHMENT0],
                              ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0], ctx);
        glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TONE_MAPPING], GL_TIMESTAMP);

        // Apply FXAA and render to screen. We use a fragment shader for this (rather than a compute
        // shader) to avoid a redundant blit.
        glQueryCounter(ctx.timer_queries_tic[TIME_STAMP_FXAA], GL_TIMESTAMP);
        if (ctx.settings.fxaa_enabled) {
            glBindVertexArray(ctx.default_vao);
            glDisable(GL_DEPTH_TEST);
            glDepthMask(GL_FALSE);

            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            draw_fxaa(ctx.programs["fxaa"],
                      ctx.fbos["tone_mapping"].attachments[GL_COLOR_ATTACHMENT0]);

            reset_gl_states();
        }
        else {
            glNamedFramebufferReadBuffer(ctx.fbos["tone_mapping"].fbo, GL_COLOR_ATTACHMENT0);
            glBlitNamedFramebuffer(ctx.fbos["tone_mapping"].fbo, 0, 0, 0, ctx.window.width,
                                   ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                                   GL_COLOR_BUFFER_BIT, GL_NEAREST);
        }
        glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_FXAA], GL_TIMESTAMP);
    }
    else if (ctx.settings.display_mode == DISPLAY_MODE_FRONT_FACES) {
        glNamedFramebufferReadBuffer(ctx.fbos["bounding_geometry"].fbo, GL_COLOR_ATTACHMENT0);
        glBlitNamedFramebuffer(ctx.fbos["bounding_geometry"].fbo, 0, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }
    else if (ctx.settings.display_mode == DISPLAY_MODE_BACK_FACES) {
        glNamedFramebufferReadBuffer(ctx.fbos["bounding_geometry"].fbo, GL_COLOR_ATTACHMENT1);
        glBlitNamedFramebuffer(ctx.fbos["bounding_geometry"].fbo, 0, 0, 0, ctx.window.width,
                               ctx.window.height, 0, 0, ctx.window.width, ctx.window.height,
                               GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }

    // Draw transfer function
    if (ctx.settings.display_transfer_function) {
        glDisable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        draw_transfer_function(ctx.programs["transfer_function"], ctx.quad_vao, ctx.tf_texture);

        reset_gl_states();
    }

    // Required for ImGui
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glQueryCounter(ctx.timer_queries_toc[TIME_STAMP_TOTAL], GL_TIMESTAMP);
    update_frame_times(ctx);
}

void error_callback(const int32_t /*error*/, const char *description)
{
    LOG_ERROR("%s\n", description);
}

void key_callback(GLFWwindow *window, const int32_t key, const int32_t scancode,
                  const int32_t action, const int32_t mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) {
        return;
    }

    if (key == GLFW_KEY_R && action == GLFW_PRESS) {
        load_shader_programs(*ctx);
    }
}

void scroll_callback(GLFWwindow *window, const double x_offset, const double y_offset)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_ScrollCallback(window, x_offset, y_offset);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    const float zoom_step = glm::radians(1.0f);
    const float min_fovy = glm::radians(1.0f);
    const float max_fovy = glm::radians(179.0f);
    if (y_offset > 0.0f) {
        ctx->camera.fovy = glm::clamp(ctx->camera.fovy - zoom_step, min_fovy, max_fovy);
    }
    else if (y_offset < 0.0f) {
        ctx->camera.fovy = glm::clamp(ctx->camera.fovy + zoom_step, min_fovy, max_fovy);
    }
}

void mouse_button_pressed(Context &ctx, const int button, const int32_t x, const int32_t y)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        gfx::trackball_start_tracking(ctx.trackball, glm::vec2(float(x), float(y)));
    }
}

void mouse_button_released(Context &ctx, const int32_t button, const int32_t /*x*/,
                           const int32_t /*y*/)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        gfx::trackball_stop_tracking(ctx.trackball);
    }
}

void mouse_button_callback(GLFWwindow *window, const int32_t button, const int32_t action,
                           const int32_t mods)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) {
        return;
    }

    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if (action == GLFW_PRESS) {
        mouse_button_pressed(*ctx, button, x, y);
    }
    else {
        mouse_button_released(*ctx, button, x, y);
    }
}

void cursor_pos_callback(GLFWwindow *window, const double x, const double y)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (ctx->trackball.tracking) {
        gfx::trackball_move(ctx->trackball, glm::vec2(float(x), float(y)));
    }
}

std::string get_file_extension(const std::string &filename)
{
    const auto pos = filename.rfind('.');
    return pos != std::string::npos ? filename.substr(pos) : "";
}

void drop_callback(GLFWwindow *window, const int32_t count, const char **paths)
{
    if (count < 1) {
        return;
    }

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    const auto filename = std::string(paths[0]);
    const std::string extension = get_file_extension(filename);
    if (extension == ".hdr") {
        glDeleteTextures(1, &ctx->longlat_envmap_texture.texture);
        ctx->longlat_envmap_texture = pbrvol::load_longlat_hdr_texture(filename);
        ctx->cubemap_texture_is_dirty = true;
    }
    else if (extension == ".vtk") {
        ctx->volume_filename = filename;
        load_volume(*ctx);
        init_camera(*ctx);
        init_key_light_camera(*ctx);
    }
}

void resize_fbos(Context &ctx, const int32_t width, const int32_t height)
{
    for (auto &fbo : ctx.fbos) {
        gfx::fbo_resize(fbo.second, width, height);
    }

    gfx::fbo_resize(ctx.fbos["bloom"], width / ctx.settings.bloom.downsampling,
                    height / ctx.settings.bloom.downsampling);
}

void framebuffer_size_callback(GLFWwindow *window, const int32_t width, const int32_t height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ctx->window.width = width;
    ctx->window.height = height;
    ctx->camera.aspect = float(width) / height;
    resize_fbos(*ctx, width, height);
    glViewport(0, 0, width, height);
}

int main(int argc, char **argv)
{
    Context ctx;

    ctx.volume_filename = argc < 2 ? root_dir() + "/data/manix.vtk" : argv[1];

    // Create GLFW window
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        std::exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    ctx.window.handle =
        glfwCreateWindow(ctx.window.width, ctx.window.height, "PBRVOL", nullptr, nullptr);
    if (!ctx.window.handle) {
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window.handle);

    glfwSetWindowUserPointer(ctx.window.handle, &ctx);
    glfwSetKeyCallback(ctx.window.handle, key_callback);
    glfwSetCursorPosCallback(ctx.window.handle, cursor_pos_callback);
    glfwSetScrollCallback(ctx.window.handle, scroll_callback);
    glfwSetMouseButtonCallback(ctx.window.handle, mouse_button_callback);
    glfwSetDropCallback(ctx.window.handle, drop_callback);
    glfwSetFramebufferSizeCallback(ctx.window.handle, framebuffer_size_callback);

    // Initialize GLEW
    glewExperimental = true;
    GLenum status = glewInit();
    if (status != GLEW_OK) {
        LOG_ERROR("%s\n", glewGetErrorString(status));
        std::exit(EXIT_FAILURE);
    }
    LOG_INFO("OpenGL version: %s\n", glGetString(GL_VERSION));

    // Initialize rendering
    init(ctx);

    // Initialize ImGUI
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(ctx.window.handle, false);
    ImGui_ImplOpenGL3_Init("#version 450");

    // Start rendering loop
    while (!glfwWindowShouldClose(ctx.window.handle)) {
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        update(ctx);
        display(ctx);
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(ctx.window.handle);
        ctx.window.frame_index += 1;
    }

    // Shut down everything
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(ctx.window.handle);
    glfwTerminate();

    return EXIT_SUCCESS;
}
