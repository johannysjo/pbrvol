#include "ao.h"
#include "logging.h"

#include <algorithm>
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <stdint.h>
#include <vector>

namespace {

uint32_t get_max_mip_level(const glm::ivec3 &dimensions)
{
    assert(dimensions[0] > 0);
    assert(dimensions[1] > 0);
    assert(dimensions[2] > 0);

    const auto max_dimension = std::max(dimensions[0], std::max(dimensions[1], dimensions[2]));
    const auto max_mip_level = static_cast<uint32_t>(std::floor(std::log2(max_dimension)));

    return max_mip_level;
}

glm::ivec3 get_mip_level_size(const glm::ivec3 &dimensions, const uint32_t level)
{
    assert(dimensions[0] > 0);
    assert(dimensions[1] > 0);
    assert(dimensions[2] > 0);

    const auto width = std::max(1, dimensions[0] / static_cast<int32_t>(std::exp2(level)));
    const auto height = std::max(1, dimensions[1] / static_cast<int32_t>(std::exp2(level)));
    const auto depth = std::max(1, dimensions[2] / static_cast<int32_t>(std::exp2(level)));

    return {width, height, depth};
}

gfx::Texture3D create_uint8_volume_texture(const glm::ivec3 &dimensions, const bool mipmap)
{
    assert(dimensions[0] > 0);
    assert(dimensions[1] > 0);
    assert(dimensions[2] > 0);

    gfx::Texture3D texture;
    texture.target = GL_TEXTURE_3D;
    texture.width = dimensions[0];
    texture.height = dimensions[1];
    texture.depth = dimensions[2];
    texture.max_level = mipmap ? get_max_mip_level(dimensions) : 0;
    texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
    const GLuint min_filter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
    texture.sampling = {min_filter, GL_LINEAR, GL_CLAMP_TO_BORDER};
    gfx::texture_3d_create(texture);
    assert(texture.texture != 0);

    return texture;
}

void generate_mipmap(const GLuint program, gfx::Texture3D &volume_texture)
{
    const glm::ivec3 dimensions = {volume_texture.height, volume_texture.width,
                                   volume_texture.depth};
    const uint32_t max_mip_level = get_max_mip_level(dimensions);

    glUseProgram(program);
    for (uint32_t i = 0; i < max_mip_level; ++i) {
        const glm::ivec3 mip_level_size = get_mip_level_size(dimensions, i + 1);
        const glm::ivec3 local_size = {4, 4, 4};
        const glm::ivec3 num_groups = mip_level_size / local_size + 1;

        glBindImageTexture(0, volume_texture.texture, i, GL_TRUE, 0, GL_READ_ONLY, GL_R8);
        glBindImageTexture(1, volume_texture.texture, i + 1, GL_TRUE, 0, GL_WRITE_ONLY, GL_R8);
        glDispatchCompute(num_groups[0], num_groups[1], num_groups[2]);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

} // namespace

namespace pbrvol {

// Takes a volume texture with voxel spacing (sx, sy, sz) as input and returns a downsampled
// version of the texture with isotropic voxel spacing 2 * min(sx, min(sy, sz)).
gfx::Texture3D downsample_volume(const std::unordered_map<std::string, GLuint> &programs,
                                 const gfx::Texture3D &src_volume_texture,
                                 const glm::vec3 &src_spacing)
{
    // Calculate voxel spacing and dimensions for the destination volume texture
    const glm::ivec3 src_dimensions = {src_volume_texture.height, src_volume_texture.width,
                                       src_volume_texture.depth};
    const glm::vec3 src_extent = src_spacing * glm::vec3(src_dimensions);
    const float min_src_spacing =
        std::fmin(src_spacing[0], std::fmin(src_spacing[1], src_spacing[2]));
    const float target_spacing = 2.0f * min_src_spacing;
    assert(target_spacing > 0.0f);
    const glm::ivec3 dst_dimensions = {std::ceil(src_extent[0] / target_spacing),
                                       std::ceil(src_extent[1] / target_spacing),
                                       std::ceil(src_extent[2] / target_spacing)};
    const glm::vec3 dst_spacing = src_extent / glm::vec3(dst_dimensions);
    LOG_INFO("Src spacing: %f %f %f\n", src_spacing[0], src_spacing[1], src_spacing[2]);
    LOG_INFO("Src dimensions: %d %d %d\n", src_dimensions[0], src_dimensions[1], src_dimensions[2]);
    LOG_INFO("Src extent: %f %f %f\n", src_extent[0], src_extent[1], src_extent[2]);
    LOG_INFO("Min spacing: %f\n", min_src_spacing);
    LOG_INFO("Target spacing: %f\n", target_spacing);
    LOG_INFO("Dst spacing: %f %f %f\n", dst_spacing[0], dst_spacing[1], dst_spacing[2]);
    LOG_INFO("Dst dimensions: %d %d %d\n", dst_dimensions[0], dst_dimensions[1], dst_dimensions[2]);

    // Downsample the source volume texture
    gfx::Texture3D dst_volume_texture = create_uint8_volume_texture(dst_dimensions, false);
    const GLuint program = programs.at("spao_downsample");
    glBindTextureUnit(0, src_volume_texture.texture);
    glBindImageTexture(0, dst_volume_texture.texture, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R8);
    glUseProgram(program);
    const glm::ivec3 local_size = {4, 4, 4};
    const glm::ivec3 num_groups = dst_dimensions / local_size + 1;
    glDispatchCompute(num_groups[0], num_groups[1], num_groups[2]);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glUseProgram(0);

    return dst_volume_texture;
}

// This function computes volumetric ambient occlusion on the GPU using the SPAO (Sparse
// Probabilistic Ambient Occlusion) method described in GPU Pro 6.
gfx::Texture3D compute_ambient_occlusion(const std::unordered_map<std::string, GLuint> &programs,
                                         const gfx::Texture3D &volume_texture,
                                         const gfx::Texture2D &opacity_tf_texture,
                                         const glm::vec3 &spacing, const float normal_factor,
                                         const float radius_mm)
{
    // Allocate 3D textures for storing opacity and ambient occlusion
    const glm::ivec3 dimensions = {volume_texture.height, volume_texture.width,
                                   volume_texture.depth};
    gfx::Texture3D ao_texture = create_uint8_volume_texture(dimensions, false);
    gfx::Texture3D opacity_texture = create_uint8_volume_texture(dimensions, true);

    { // Map voxel intensity to opacity
        const GLuint program = programs.at("spao_classify");
        glBindTextureUnit(0, volume_texture.texture);
        glBindTextureUnit(1, opacity_tf_texture.texture);
        glBindImageTexture(0, opacity_texture.texture, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R8);
        glUseProgram(program);
        const glm::ivec3 local_size = {8, 8, 8};
        const glm::ivec3 num_groups = dimensions / local_size + 1;
        glDispatchCompute(num_groups[0], num_groups[1], num_groups[2]);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
        glUseProgram(0);
    }

    // Generate opacity mipmaps
    generate_mipmap(programs.at("spao_mipmap"), opacity_texture);

    { // Compute volumetric ambient occlusion from the opacity mipmaps
        const GLuint program = programs.at("spao_ao");
        glProgramUniform1f(program, 0, normal_factor);
        glProgramUniform3fv(program, 1, 1, &spacing[0]);
        glProgramUniform1f(program, 2, radius_mm);
        glBindTextureUnit(0, opacity_texture.texture);
        glBindImageTexture(0, ao_texture.texture, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R8);
        glUseProgram(program);
        const glm::ivec3 local_size = {8, 8, 8};
        const glm::ivec3 num_groups = dimensions / local_size + 1;
        glDispatchCompute(num_groups[0], num_groups[1], num_groups[2]);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
        glUseProgram(0);
    }

    glDeleteTextures(1, &opacity_texture.texture);

    return ao_texture;
}

} // namespace pbrvol
